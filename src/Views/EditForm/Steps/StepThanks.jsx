import React, {useContext} from 'react'
/*Context */
import { EditFormContext } from "../EditFormContext";
/*Icons */
import IosArrowBack from 'react-ionicons/lib/IosArrowBack'
import MdCheckmarkCircleOutline from 'react-ionicons/lib/MdCheckmarkCircleOutline'

function StepThanks() {
  const context = useContext(EditFormContext);
  const actions = context.actions;
  const { startResetStep } = actions

  
  return (
    <div>
      <div className="step_thanks" >
        <span >
          <MdCheckmarkCircleOutline color="#43853d" />
          <strong>
            Los datos han sido actualizados con éxito
          </strong>
        </span>
      </div>
      <div className="col-12">
        <div className="container_btns_step">
          <button 
          className="btn btn-warning"
          onClick={() => startResetStep()}
          >
            <IosArrowBack fontSize="30px" color="black"  className='btn-step__next' />
            Volver a actualizar datos
          </button>
        </div>
      </div>
    </div>
  )
}

export default StepThanks
