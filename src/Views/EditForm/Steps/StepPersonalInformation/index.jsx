import React from "react";
import {
  toInputUppercase,
  toInputUppercaseWhitNumbers,
  toInputLimitCharactersNumbers,
  toInputLimitCharactersLocalPhone,
  toInputUppercaseWhitNumbersAndSpace,
  toInputLimitTwoCharactersNumbers
} from "../../../../Utils/inputUppercase";

import { useStepPersonalInformation } from "./useStepPersonalInformation";

/*Form Data */
import {
  RHS,
  TypeContracts,
  civilStatus,
  TypeTransports,
} from "../../FormStorage";

import ErrorField from "../../../../Components/Form/ErrorField";
import AddressField from "../../../../Components/Form/addressField";

/*Boostrap Components */
import { Input } from "reactstrap";

/*Components */
import ButtonNextStep from "../../../../Components/ButtonNextStep";

function StepPersonalInformation() {
  const { user, changeListCities, cities, departments, formik } =
    useStepPersonalInformation();

  const { values, errors, touched, handleChange, handleBlur, setFieldValue } =
    formik;

  return (
    <div className="information_personal">
      <form onSubmit={formik.handleSubmit} className="">
        <div className="information_personal__container">
          <div className="items gender_container">
            <label htmlFor="">Sexo(*) </label>
            <br />
            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="gender"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="MALE"
                checked={values.gender === "MALE" && true}
              />
              <label className="form-check-label">MASCULINO</label>
            </div>

            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="gender"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="FEMALE"
                checked={values.gender === "FEMALE" && true}
              />
              <label className="form-check-label">FEMENINO</label>
            </div>

            <ErrorField touched={touched.gender} message={errors.gender} />
          </div>

          <div className="items">
            <label htmlFor="">RH (*)</label>
            <select
              className={
                touched.id_card_rh && errors.id_card_rh
                  ? "form-control has-error"
                  : "form-control"
              }
              name="id_card_rh"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.id_card_rh != null ? values.id_card_rh : ""}
            >
              <option></option>
              {RHS.map((rh, i) => (
                <option key={i} value={rh}>
                  {rh}
                </option>
              ))}
            </select>
            <ErrorField
              touched={touched.id_card_rh}
              message={errors.id_card_rh}
            />
          </div>

          <div className="items">
            <label htmlFor="">Fecha de nacimiento (*)</label>
            <Input
              type="date"
              name="birth_date"
              className={
                touched.birth_date && errors.birth_date
                  ? "form-control has-error"
                  : "form-control"
              }
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.birth_date}
            />
            <ErrorField
              touched={touched.birth_date}
              message={errors.birth_date}
            />
          </div>

          {values.gender === "MALE" && (
            <div className="items">
              <label htmlFor="">Libreta militar</label>
              <input
                className={
                  touched.military_card && errors.military_card
                    ? "form-control has-error"
                    : "form-control"
                }
                name="military_card"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.military_card}
                onInput={toInputUppercaseWhitNumbers}
                type="text"
              />
              <ErrorField
                touched={touched.military_card}
                message={errors.military_card}
              />
            </div>
          )}

          {values.gender === "MALE" && (
            <div className="items">
              <label htmlFor="">Clase</label>
              <input
                className={
                  touched.military_card_class && errors.military_card_class
                    ? "form-control has-error"
                    : "form-control"
                }
                name="military_card_class"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.military_card_class}
                onInput={toInputUppercaseWhitNumbers}
                type="text"
              />
              <ErrorField
                touched={touched.military_card_class}
                message={errors.military_card_class}
              />
            </div>
          )}

          {values.gender === "MALE" && (
            <div className="items">
              <label htmlFor="">Distrito</label>
              <input
                className={
                  touched.military_card_district &&
                  errors.military_card_district
                    ? "form-control has-error"
                    : "form-control"
                }
                name="military_card_district"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.military_card_district}
                onInput={toInputUppercaseWhitNumbers}
                type="text"
              />
              <ErrorField
                touched={touched.military_card_district}
                message={errors.military_card_district}
              />
            </div>
          )}

          <div className="">
            <label htmlFor="">Tipo de empleado (*)</label>
            <select
              className={
                touched.type_employee && errors.type_employee
                  ? "form-control has-error"
                  : "form-control"
              }
              name="type_employee"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.type_employee}
            >
              <option></option>
              {TypeContracts.map((typeContract, i) => (
                <option key={i} value={typeContract.value}>
                  {typeContract.name}
                </option>
              ))}
            </select>
            <ErrorField
              touched={touched.type_employee}
              message={errors.type_employee}
            />
          </div>

          <div>
            <label htmlFor="">Documento de identidad (*)</label>
            <input
              type="text"
              className="form-control"
              value={user?.id_card}
              disabled
            />
          </div>

          <div className="items">
            <label htmlFor="">Lugar de expedición (*)</label>
            <input
              className={
                touched.id_card_expedition_place &&
                errors.id_card_expedition_place
                  ? "form-control has-error"
                  : "form-control"
              }
              name="id_card_expedition_place"
              onChange={handleChange}
              onInput={toInputUppercase}
              onBlur={handleBlur}
              value={values.id_card_expedition_place}
              type="text"
            />
            <ErrorField
              touched={touched.id_card_expedition_place}
              message={errors.id_card_expedition_place}
            />
          </div>
          <div className="itmes">
            <label htmlFor="">Estado Civil (*)</label>
            <select
              className={
                touched.civil_status && errors.civil_status
                  ? "form-control has-error"
                  : "form-control"
              }
              name="civil_status"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.civil_status != null ? values.civil_status : ""}
            >
              <option></option>
              {civilStatus.map((status, i) => (
                <option key={i} value={status.value}>
                  {status.name}
                </option>
              ))}
            </select>
            <ErrorField
              touched={touched.civil_status}
              message={errors.civil_status}
            />
          </div>

          <div className="items have_sons_container">
            <label htmlFor="">Tienes hijos? (*)</label>
            <br />
            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="have_sons"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value={1}
                checked={values.have_sons == 1 ? true : ""}
              />
              <label className="form-check-label">Sí</label>
            </div>

            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="have_sons"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value={0}
                checked={values.have_sons == 0 ? true : ""}
              />
              <label className="form-check-label">No</label>
            </div>

            <ErrorField
              touched={touched.have_sons}
              message={errors.have_sons}
            />
          </div>

          {values.have_sons == 1 && (
            <div className="">
              <label htmlFor="">Número de hijos (*)</label>
              <input
                className={
                  touched.child_number && errors.child_number
                    ? "form-control has-error"
                    : "form-control"
                }
                name="child_number"
                onChange={handleChange}
                onBlur={handleBlur}
                onInput={toInputLimitTwoCharactersNumbers}
                value={values.have_sons == 1 ? values.child_number : 0}
                type="number"
              />
              <ErrorField
                touched={touched.child_number}
                message={errors.child_number}
              />
            </div>
          )}

          <div className="items">
            <AddressField
              name="address"
              value={values.address}
              onChange={handleChange}
              onBlur={handleBlur}
              touched={touched.address}
              message={errors.address}
              setFieldValue={setFieldValue}
            />
            <ErrorField touched={touched.address} message={errors.address} />
          </div>

          <div className="">
            <label htmlFor="">Departamento (*)</label>
            <select
              className={
                touched.residence_department && errors.residence_department
                  ? "form-control has-error"
                  : "form-control"
              }
              name="residence_department"
              onChange={(e) => {
                handleChange(e);
                changeListCities(e.target.value);
              }}
              onBlur={handleBlur}
              value={values.residence_department}
            >
              <option></option>
              {departments.map((department, i) => (
                <option key={i} value={department.departamento.toUpperCase()}>
                  {department.departamento.toUpperCase()}
                </option>
              ))}
            </select>
            <ErrorField
              touched={touched.residence_department}
              message={errors.residence_department}
            />
          </div>

          <div className="">
            <label htmlFor="">Ciudad/Municipio (*)</label>
            <select
              className={
                touched.city && errors.city
                  ? "form-control has-error"
                  : "form-control"
              }
              name="city"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.city}
            >
              <option></option>
              {cities.map((city, i) => (
                <option key={i} value={city}>
                  {city}
                </option>
              ))}
            </select>
            <ErrorField touched={touched.city} message={errors.city} />
          </div>

          <div className="">
            <label htmlFor="">Barrio (*)</label>
            <input
              className={
                touched.neighborhood && errors.neighborhood
                  ? "form-control has-error"
                  : "form-control"
              }
              name="neighborhood"
              onInput={toInputUppercaseWhitNumbersAndSpace}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.neighborhood}
              type="text"
            />
            <ErrorField
              touched={touched.neighborhood}
              message={errors.neighborhood}
            />
          </div>

          <div className="gender_container">
            <label htmlFor="">Tipo de vivienda (*) </label> <br />
            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="home_type"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="OWN"
                checked={values.home_type === "OWN" && true}
              />
              <label className="form-check-label">PROPIA</label>
            </div>

            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="home_type"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="RENT"
                checked={values.home_type === "RENT" && true}
              />
              <label className="form-check-label">ALQUILADA</label>
            </div>

            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="home_type"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="CREDIT"
                checked={values.home_type === "CREDIT" && true}
              />
              <label className="form-check-label">CON CRÉDITO</label>
            </div>

            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="home_type"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="FAMILIAR"
                checked={values.home_type === "FAMILIAR" && true}
              />
              <label className="form-check-label">FAMILIAR</label>
            </div>

            <ErrorField
              touched={touched.home_type}
              message={errors.home_type}
            />
          </div>

          <div className="">
            <label htmlFor="">Teléfono</label>

            <input
              className={
                touched.local_phone && errors.local_phone
                  ? "form-control has-error"
                  : "form-control"
              }
              name="local_phone"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.local_phone}
              onInput={toInputLimitCharactersLocalPhone}
              type="text"
            />

            <ErrorField
              touched={touched.local_phone}
              message={errors.local_phone}
            />
          </div>

          <div className="">
            <label htmlFor="">Celular(*)</label>
            <input
              className={
                touched.phone && errors.phone
                  ? "form-control has-error"
                  : "form-control"
              }
              name="phone"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.phone}
              onInput={toInputLimitCharactersNumbers}
              type="text"
            />
            <ErrorField touched={touched.phone} message={errors.phone} />
          </div>

          <div className="">
            <label htmlFor="">Email (*)</label>
            <input
              className={
                touched.email && errors.email
                  ? "form-control has-error"
                  : "form-control"
              }
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              type="text"
            />
            <ErrorField touched={touched.email} message={errors.email} />
          </div>

          <div className="items">
            <label htmlFor="">
              Medio utilizado para llegar a New Stetic (*)
            </label>
            <select
              className={
                touched.private_transport && errors.private_transport
                  ? "form-control has-error"
                  : "form-control"
              }
              name="private_transport"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.private_transport}
            >
              <option></option>
              {TypeTransports.map((TypeTransport, i) => (
                <option key={i} value={TypeTransport.value}>
                  {TypeTransport.name}
                </option>
              ))}
            </select>
            <ErrorField
              touched={touched.private_transport}
              message={errors.private_transport}
            />
          </div>

          <div className="gender_container">
            <label htmlFor="">Se considera usted fumador (*)</label>
            <br />
            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="smoker"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="NONE"
                checked={values.smoker === "NONE" && true}
              />
              <label className="form-check-label">NO FUMADOR</label>
            </div>

            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="smoker"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="SOMETIMES"
                checked={values.smoker === "SOMETIMES" && true}
              />
              <label className="form-check-label">FUMADOR OCASIONAL</label>
            </div>

            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="smoker"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="FREQUENT"
                checked={values.smoker === "FREQUENT" && true}
              />
              <label className="form-check-label">FUMADOR FRECUENTE</label>
            </div>

            <ErrorField touched={touched.smoker} message={errors.smoker} />
          </div>

          <div className=" gender_container">
            <label htmlFor="">Se condidera usted bebedor (*) </label>
            <br />
            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="alcoholic"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="NONE"
                checked={values.alcoholic === "NONE" && true}
              />
              <label className="form-check-label">NO BEBEDOR</label>
            </div>

            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="alcoholic"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="SOMETIMES"
                checked={values.alcoholic === "SOMETIMES" && true}
              />
              <label className="form-check-label">BEBEDOR OCASIONAL</label>
            </div>

            <div className="form-check form-check-inline ">
              <input
                className="form-check-input"
                name="alcoholic"
                type="radio"
                onChange={handleChange}
                onBlur={handleBlur}
                value="FREQUENT"
                checked={values.alcoholic === "FREQUENT" && true}
              />
              <label className="form-check-label">BEBEDOR FRECUENTE</label>
            </div>

            <ErrorField
              touched={touched.alcoholic}
              message={errors.alcoholic}
            />
          </div>
        </div>

        <div className="col-12">
          <br/>

          <div className="container_btns_step">
            <ButtonNextStep type="submit" />
          </div>
        </div>
      </form>
    </div>
  );
}

export default StepPersonalInformation;
