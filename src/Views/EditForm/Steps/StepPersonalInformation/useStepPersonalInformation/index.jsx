import { useState, useContext, useEffect } from "react";

/*Context */
import { EditFormContext } from "../../../EditFormContext";

/* Formik */
import { useFormik } from "formik";
import ValidationSchema from "../../Validations/ValidationStepPersonailInformation";

export const useStepPersonalInformation = () => {
  const context = useContext(EditFormContext);
  const [cities, setCities] = useState([]);
  const state = context.state;
  const actions = context.actions;
  const { main, user, departments, userWP } = state;

  const changeListCities = (departmentSelect, values = []) => {
    let listCities = [];
    departments
      .filter(department => {
        return department.departamento.toUpperCase() === departmentSelect.toUpperCase();
      })
      .map(department => {
        department.ciudades.map(city => {
          listCities.push(city.toUpperCase());
        });
      });
    setCities(listCities);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (user?.residence_department !== null)
      changeListCities(user.residence_department.toUpperCase());
  }, [ departments ]);

  const formik = useFormik({
    validationSchema:ValidationSchema ,
    initialValues: {
      fk_user_id: user?.fk_user_id,
      gender: user?.gender,
      id_card_rh: user?.id_card_rh,
      birth_date: user?.birth_date,
      civil_status: user?.civil_status,
      type_employee: user.type_employee,
      id_card_expedition_place: user?.id_card_expedition_place,
      child_number: user?.child_number != null ? user.child_number : "",
      have_sons: user?.child_number > 0 ? 1 : 0,
      address: user?.address,
      neighborhood: user?.neighborhood,
      residence_department: user?.residence_department?.toUpperCase() ? user.residence_department.toUpperCase() : '',
      city: user?.city ? user.city.toUpperCase() : '',
      phone: user?.phone != null ? user.phone : "",
      local_phone: user?.local_phone != null ? user.local_phone : "",
      email: userWP?.user_email != null ? userWP.user_email : "",
      private_transport:
        user?.private_transport != null ? user.private_transport : "",
      smoker: user?.smoker != null ? user.smoker : "",
      alcoholic: user?.alcoholic != null ? user.alcoholic : "",
      home_type: user ? user.home_type : "",
      military_card: user?.military_card != null ? user.military_card : "",
      military_card_class:
        user?.military_card_class != null ? user.military_card_class : "",
      military_card_district:
        user?.military_card_district != null ? user.military_card_district : ""
    },
    onSubmit: values => {
      if(values.gender === 'female'){
        values.military_card = ''
        values.military_card_class = ''
        values.military_card_district = ''
      }

      if(values.have_sons === 0){
        values.child_number = 0
      }
      values.local_phone = String(values.local_phone)
      values.phone = String(values.phone)
      actions.startUpdateUser(values, main + 1);
    }
  });

  return {
    user,
    userWP,
    changeListCities,
    cities,
    main,
    actions,
    departments,
    formik
  };
};
