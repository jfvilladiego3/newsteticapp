import React, { useContext, useEffect, useState } from "react";
/*Context */
import { EditFormContext } from "../../../EditFormContext";
/*Components */
import ListHeader from "./ListDefault/ListHeader";
import ListContent from "./ListDefault/ListContent";
import ListContentMovile from "./ListDefault/ListContentMovile";
import ListContentForm from "./ListDefault/ListContentForm";

import MdAdd from "react-ionicons/lib/MdAdd";

function StepFamiliarList() {
  const context = useContext(EditFormContext);
  const state = context.state;
  const actions = context.actions;
  const { familiars, setFamiliars } = state;
  const { startDeleteFamiliar } = actions;
  const [editFamiliar, setEditFamiliar] = useState(null);
  const [createFamiliar, setCreateFamiliar] = useState(null);

  return (
    <div className="familiar_step_list ">
      <div className=" item-right">
        <button
          onClick={() => setCreateFamiliar(familiars.length)}
          type="button"
          className="btn btn-warning "
        >
          Agregar
          <MdAdd fontSize="20px" color="black" />
        </button>
      </div>
      <ListHeader />
      {familiars.map((familiar, i) => (
        <div key={i} className="">
          {editFamiliar != i && (
            <ListContent
              key={i}
              index={i}
              familiar={familiar}
              removeItem={startDeleteFamiliar}
              setEditFamiliar={setEditFamiliar}
            />
          )}
          {editFamiliar == i && (
            <ListContentForm
              key={i}
              index={i}
              familiar={familiar}
              setEditFamiliar={setEditFamiliar}
            />
          )}
        </div>
      ))}
      {createFamiliar == familiars.length && (
        <ListContentForm
          key={familiars.length}
          index={familiars.length}
          familiar={{}}
          setEditFamiliar={setCreateFamiliar}
        />
      )}
    </div>
  );
}

export default StepFamiliarList;
