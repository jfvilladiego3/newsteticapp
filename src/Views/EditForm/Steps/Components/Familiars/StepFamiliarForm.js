import React, { useContext, useState, useEffect, useRef } from "react";
/*Formik and yup */
import { Formik, FieldArray, Field } from "formik";
/* Validations */
import ValidationSchema from "../../../Steps/Validations/ValidationStepFamiliar";
/*Context */
import { EditFormContext } from "../../../EditFormContext";

/*Components */
import ErrorField from "../../../../../Components/ErrorsField";

function StepFamiliarForm() {
  const { saveFamiliars, setFamiliars, familiars, user } =
    useContext(EditFormContext);

  return (
    <Formik
      initialValues={{
        complete_name: "",
        age: "",
        relationship: "",
        occupation: "",
        economic_dependency: "",
        education_level: "",
      }}
      validationSchema={ValidationSchema}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        saveFamiliars({
          fk_user_id: user.fk_user_id,
          complete_name: values.complete_name,
          age: values.age,
          relationship: values.relationship,
          occupation: values.occupation,
          economic_dependency: values.economic_dependency == "1" ? 1 : 0,
          education_level: values.education_level,
        }).then((data) => {
          setFamiliars([
            ...familiars,
            {
              id: data.familiars.id,
              complete_name: values.complete_name,
              age: values.age,
              relationship: values.relationship,
              occupation: values.occupation,
              economic_dependency: values.economic_dependency == "1" ? 1 : 0,
              education_level: values.education_level,
            },
          ]);
          resetForm({});
        });
      }}
    >
      {({
        values,
        errors,
        touched,
        isValid,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        setFieldValue,
      }) => (
        <form onSubmit={handleSubmit} className="row familiars_form">
          <div className="familiars_form__container_title col-12">
            <h5 className="familiars_form__title">Información Familiar</h5>
          </div>
          <span className="col-12 familiars_form__sub_title">
            Ingresa los datos de las personas con las que convives. Digite N/A
            para los familires que no tienen grado de escolaridad
          </span>

          <div className="col-xs-12 col-md-6">
            <label htmlFor="">Nombre Completo</label>
            <input
              className={
                touched.complete_name && errors.complete_name
                  ? "form-control has-error"
                  : "form-control"
              }
              name="complete_name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.complete_name}
              type="text"
            />
            <ErrorField
              touched={touched.complete_name}
              message={errors.complete_name}
            />
          </div>

          <div className="col-xs-12 col-md-6">
            <label htmlFor="">Edad</label>
            <input
              className={
                touched.age && errors.age
                  ? "form-control has-error"
                  : "form-control"
              }
              name="age"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.age}
              type="text"
            />
            <ErrorField touched={touched.age} message={errors.age} />
          </div>

          <div className="col-xs-12 col-md-6">
            <label htmlFor="">Parentesco</label>
            <input
              className={
                touched.relationship && errors.relationship
                  ? "form-control has-error"
                  : "form-control"
              }
              name="relationship"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.relationship}
              type="text"
            />
            <ErrorField
              touched={touched.relationship}
              message={errors.relationship}
            />
          </div>

          <div className="col-xs-12 col-md-6">
            <label htmlFor="">Ocupación</label>
            <input
              className={
                touched.occupation && errors.occupation
                  ? "form-control has-error"
                  : "form-control"
              }
              name="occupation"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.occupation}
              type="text"
            />
            <ErrorField
              touched={touched.occupation}
              message={errors.occupation}
            />
          </div>

          <div className="col-xs-12 col-md-6">
            <label htmlFor="">Depende económicamente de usted?</label>
            <select
              className={
                touched.economic_dependency && errors.economic_dependency
                  ? "form-control has-error"
                  : "form-control"
              }
              name="economic_dependency"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.economic_dependency}
            >
              <option></option>
              <option value={1}>SI</option>
              <option value={0}>NO</option>
            </select>
            <ErrorField
              touched={touched.economic_dependency}
              message={errors.economic_dependency}
            />
          </div>

          <div className="col-xs-12 col-md-6">
            <label htmlFor="">Último nivel educativo</label>
            <input
              className={
                touched.education_level && errors.education_level
                  ? "form-control has-error"
                  : "form-control"
              }
              name="education_level"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.education_level}
              type="text"
            />
            <ErrorField
              touched={touched.education_level}
              message={errors.education_level}
            />
          </div>

          <div className="col-12">
            <div className="">
              <button className="btn btn-success" type="submit">
                Guardar
              </button>
            </div>
          </div>
        </form>
      )}
    </Formik>
  );
}

export default StepFamiliarForm;
