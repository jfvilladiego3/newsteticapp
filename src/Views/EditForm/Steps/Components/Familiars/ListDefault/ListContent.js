import React, { useState } from "react";

/*Icons */
import IosTrashOutline from "react-ionicons/lib/IosTrashOutline";
import MdCreate from "react-ionicons/lib/MdCreate";

/*Components */
import ModalContainerDelete from "../../../../../../Components/Modals/ModalContainerDelete";

function ListContent(props) {
  const [show, setShow] = useState(false);

  const {
    id,
    complete_name,
    age,
    relationship,
    occupation,
    economic_dependency,
    education_level,
    is_roomie,
  } = props.familiar;

  const { removeItem, setEditFamiliar, index } = props;
  const [hover, setHover] = useState(false);

  return (
    <div
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      className={
        hover
          ? "familiar_step_list__container fade-in hover_list"
          : "familiar_step_list__container fade-in"
      }>
      <div className="familiar_step_list__item">
        <span className="header-list">
          <strong>Nombre:</strong>
        </span>
        <span>{complete_name.toUpperCase()}</span>
      </div>

      <div className="familiar_step_list__item">
        <span className="header-list">
          <strong>Edad:</strong>
        </span>
        <span>{age}</span>
      </div>

      <div className="familiar_step_list__item">
        <span className="header-list">
          <strong>Parentesco:</strong>
        </span>
        <span>{relationship.toUpperCase()}</span>
      </div>

      <div className="familiar_step_list__item">
        <span className="header-list">
          <strong>Estudio:</strong>
        </span>
        <span>{education_level.toUpperCase()}</span>
      </div>

      <div className="familiar_step_list__item">
        <span className="header-list">
          <strong>Ocupación:</strong>
        </span>
        <span>{occupation.toUpperCase()}</span>
      </div>

      <div className="familiar_step_list__item">
        <span className="header-list">
          <strong>¿Viven con usted?</strong>
        </span>
        {is_roomie == 0 &&<span>NO</span>}
        {is_roomie == 1 &&<span>SÍ</span>}
      </div>

      <div className="familiar_step_list__item">
        <span className="header-list">
          <strong>Depende económicamente de usted:</strong>
        </span>
        {economic_dependency == 0 && <span>NO</span>}
        {economic_dependency == 1 && <span>SÍ</span>}
      </div>

      <div className="familiar_step_list__actions">
        <div>
          <IosTrashOutline
            onClick={() => setShow(true)}
            fontSize="45px"
            color="#dc3545"
            className="circle-icon"
          />

          <ModalContainerDelete
            identification={id}
            index={index}
            show={show}
            setShow={setShow}
            removeItem={removeItem}
            title={"Eliminar Familiar"}
            message={"Realmente desea eliminar este dato?"}
          />
        </div>

        <MdCreate
          onClick={() => setEditFamiliar(index)}
          fontSize="45px"
          color="#dc3545"
          className="circle-icon"
        />
      </div>
    </div>
  );
}

export default ListContent;
