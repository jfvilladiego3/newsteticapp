import React, {useState} from 'react'

/*Icons */
import IosTrashOutline from 'react-ionicons/lib/IosTrashOutline'
import MdCreate from 'react-ionicons/lib/MdCreate'

/*Components */
import ModalContainerDelete from '../../../../../../Components/Modals/ModalContainerDelete'
import ListHeaderMovile from './ListHeaderMovile'


function ListContentMovile(props) {
  const [show, setShow] = useState(false)
  
  const { 
    id,
    complete_name, 
    age, 
    relationship, 
    occupation, 
    economic_dependency ,
    education_level
  } = props.familiar

  const {removeItem, setEditFamiliar, index} = props
  const [hover, setHover] = useState(false)

  return (
    <div  className="familiar_container_mobile" >
      
      <div 
      onMouseEnter={() => setHover(true)} 
      onMouseLeave={() =>setHover(false)}
      className={hover ? 'familiar_step_list__container_mobile fade-in hover_list': 'familiar_step_list__container_mobile fade-in' }>
          <div className="familiar_step_list__item" > 
            <span>
              <strong>
                Nombre:
              </strong>
            </span>
            <span>
              {complete_name.toUpperCase()}
            </span>
          </div>
          <div className="familiar_step_list__item" > 
            <span>
              <strong>
                Edad:
              </strong>
            </span>
            <span>
              {age}
            </span>
          </div>
          <div className="familiar_step_list__item" >
            <span>
              <strong>
                Parentesco:
              </strong>
            </span>
            <span>
              {relationship.toUpperCase()}
            </span> 
          </div>
          <div className="familiar_step_list__item" >
            <span>
              <strong>
                Estudio:
              </strong>
            </span>
            <span>
              {education_level.toUpperCase()} 
            </span> 
          </div>
          <div className="familiar_step_list__item" >
            <span>
              <strong>
                Ocuapación:
              </strong>
            </span>
            <span>
              {occupation.toUpperCase()} 
            </span> 
          </div>
          <div className="familiar_step_list__item" >
            <span>
              <strong>
                Depende económicamente de usted:
              </strong>
            </span>
            {economic_dependency == 0 &&  <span>
              NO 
            </span> }
            {economic_dependency == 1 &&  <span>
              SÍ 
            </span> }
          </div>
          <div className="familiar_step_list__actions" >
            
            <div>
              <IosTrashOutline
              // onClick={ () => removeItem(id, index)} 
              onClick={ () => setShow(true) }
              fontSize="45px" color='#dc3545'   
              className='circle-icon' />
              <ModalContainerDelete
              // onClick={ () => removeItem(id, index)} 
              identification={id}
              index={index}   
              show={show}
              setShow={setShow}
              removeItem={removeItem}
              title={'Eliminar Familiar'}
              message={'Realmente desea eliminar este dato?'}
              />
            </div>
            
            <MdCreate
            onClick={ () => setEditFamiliar(index)} 
            fontSize="45px" color='#dc3545'   
            className='circle-icon' />
          </div>
      </div>
      
    </div>
  )
}

export default ListContentMovile
