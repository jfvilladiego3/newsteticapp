import React from "react";

function ListHeader() {
  return (
    <div className="familiar_step_list__headers fade-in header-content">
      <div className="familiar_step_list__header_item">
        <p>
          <strong>Nombre</strong>
        </p>
      </div>
      <div className="familiar_step_list__header_item">
        <p>
          <strong>Edad</strong>
        </p>
      </div>
      <div className="familiar_step_list__header_item">
        <p>
          <strong>Parentesco</strong>
        </p>
      </div>
      <div className="familiar_step_list__header_item">
        <p>
          <strong>Último grado aprobado</strong>
        </p>
      </div>
      <div className="familiar_step_list__header_item">
        <p>
          <strong>Ocupación</strong>
        </p>
      </div>
      <div className="familiar_step_list__header_item">
        <p>
          <strong>¿Viven con usted?</strong>
        </p>
      </div>
      <div className="familiar_step_list__header_item">
        <p>
          <strong>Depende económicamente de usted</strong>
        </p>
      </div>
    </div>
  );
}

export default ListHeader;
