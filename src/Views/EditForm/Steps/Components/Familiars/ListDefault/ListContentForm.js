import React, { useState, useContext, useEffect } from "react";

import {
  toInputUppercase,
  toInputUppercaseWhitNumbers,
  toInputLimitTwoCharactersNumbers,
} from "../../../../../../Utils/inputUppercase";
/*Context */
import { EditFormContext } from "../../../../EditFormContext";
/* Formik */
import { useFormik } from "formik";
/*FormStorage */
import {
  TypeEducations,
  TypeRelationship,
  TypeOcupation,
} from "../../../../FormStorage";
import ValidationSchema from "../../../Validations/ValidationStepFamiliar";
import ErrorField from "../../../../../../Components/Form/ErrorField";

/*Icons */
import IosCheckmark from "react-ionicons/lib/IosCheckmark";
import IosClose from "react-ionicons/lib/IosClose";

function ListContentForm(props) {
  const context = useContext(EditFormContext);
  const state = context.state;
  const actions = context.actions;

  const { user } = state;
  const { startCreateFamiliar, startEditFamiliar } = actions;

  const {
    id,
    complete_name,
    age,
    relationship,
    occupation,
    economic_dependency,
    education_level,
    is_roomie
  } = props.familiar;

  const { setEditFamiliar, index } = props;
  const [hover, setHover] = useState(false);

  const formik = useFormik({
    validationSchema: ValidationSchema,
    initialValues: {
      fk_user_id: user.fk_user_id,
      id: id ? id : null,
      complete_name: complete_name ? complete_name.toUpperCase() : "",
      age: age ? age : "",
      relationship: relationship ? relationship.toUpperCase() : "",
      occupation: occupation ? occupation.toUpperCase() : "",
      economic_dependency: economic_dependency ? economic_dependency : "",
      education_level: education_level ? education_level : "",
      is_roomie: is_roomie ? is_roomie : ''
    },
    onSubmit: (values) => {
      setEditFamiliar(null);
      if (id && id != undefined && id != null) {
        startEditFamiliar(id, values, index);
        setEditFamiliar(null);
      } else {
        startCreateFamiliar(values);
        setEditFamiliar(null);
      }
    },
  });

  const {
    values,
    errors,
    touched,
    isValid,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
    setFieldValue,
  } = formik;

  return (
    <form
      onSubmit={formik.handleSubmit}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      className={
        hover
          ? "familiar_step_list__container fade-in hover_list"
          : "familiar_step_list__container fade-in"
      }
    >
      <div className="familiar_step_list__item">
        <label htmlFor="" className="header-list">
          Nombre
        </label>
        <div className="">
          <input
            className={
              touched.complete_name && errors.complete_name
                ? "form-control has-error"
                : "form-control"
            }
            name="complete_name"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.complete_name}
            onInput={toInputUppercase}
            type="text"
          />
          <ErrorField
            touched={touched.complete_name}
            message={errors.complete_name}
          />
        </div>
      </div>
      <div className="familiar_step_list__item">
        <label htmlFor="" className="header-list">
          Edad
        </label>
        <input
          className={
            touched.age && errors.age
              ? "form-control has-error"
              : "form-control"
          }
          name="age"
          onInput={toInputLimitTwoCharactersNumbers}
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.age}
          type="text"
        />
        <ErrorField touched={touched.age} message={errors.age} />
      </div>
      <div className="familiar_step_list__item">
        <label htmlFor="" className="header-list">
          Parentesco
        </label>
        <select
          className={
            touched.relationship && errors.relationship
              ? "form-control has-error"
              : "form-control"
          }
          name="relationship"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.relationship}
          type="text"
        >
          <option></option>
          {TypeRelationship.map((relation) => (
            <option value={relation.value}> {relation.name} </option>
          ))}
        </select>
        <ErrorField
          touched={touched.relationship}
          message={errors.relationship}
        />
      </div>
      <div className="familiar_step_list__item">
        <label htmlFor="" className="header-list">
          Último grado aprobado
        </label>
        <select
          className={
            touched.education_level && errors.education_level
              ? "form-control has-error"
              : "form-control"
          }
          name="education_level"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.education_level}
        >
          <option></option>
          {TypeEducations.map((education) => (
            <option value={education.value}> {education.name} </option>
          ))}
        </select>
        <ErrorField
          touched={touched.education_level}
          message={errors.education_level}
        />
      </div>
      <div className="familiar_step_list__item">
        <label htmlFor="" className="header-list">
          Ocupación
        </label>
        <select
          className={
            touched.occupation && errors.occupation
              ? "form-control has-error"
              : "form-control"
          }
          name="occupation"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.occupation}
          type="text"
        >
          <option></option>
          {TypeOcupation.map((occupation) => (
            <option value={occupation.value}> {occupation.name} </option>
          ))}
        </select>
        <ErrorField touched={touched.occupation} message={errors.occupation} />
      </div>

      

      {/* #TODO */}
      <div className="familiar_step_list__item">
        <label htmlFor="" className="header-list">
          ¿Viven con usted?
        </label>
        <select
          className={
            touched.is_roomie && errors.is_roomie
              ? "form-control has-error"
              : "form-control"
          }
          name="is_roomie"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.is_roomie}
        >
          <option></option>
          <option value={1}>SI</option>
          <option value={0}>NO</option>
        </select>
        <ErrorField
          touched={touched.is_roomie}
          message={errors.is_roomie}
        />
      </div>



      <div className="familiar_step_list__item">
        <label htmlFor="" className="header-list">
          Depende económicamente de usted
        </label>
        <select
          className={
            touched.economic_dependency && errors.economic_dependency
              ? "form-control has-error"
              : "form-control"
          }
          name="economic_dependency"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.economic_dependency}
        >
          <option></option>
          <option value={1}>SI</option>
          <option value={0}>NO</option>
        </select>
        <ErrorField
          touched={touched.economic_dependency}
          message={errors.economic_dependency}
        />
      </div>

      

      <div className="familiar_step_list__actions">
        <button
          type="submit"
          className="btn btn-success circle-button btn-shadow"
        >
          <IosCheckmark
            fontSize="40px"
            color="white"
            className="circle-btn-icon"
          />
        </button>
        <IosClose
          onClick={() => setEditFamiliar(null)}
          fontSize="45px"
          color="white"
          className="circle-icon danger"
        />
      </div>
    </form>
  );
}

export default ListContentForm;
