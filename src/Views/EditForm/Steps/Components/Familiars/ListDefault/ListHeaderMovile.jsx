import React from 'react'


function ListHeaderMovile() {
  return (
    <div className='familiar_step_list__headers_movile fade-in'>
        <div className="familiar_step_list__header_item" > 
          <p>
            <strong>
              Nombre
            </strong>
          </p>
        </div>
        <div className="familiar_step_list__header_item" > 
          <p>
            <strong>
              Edad
            </strong>
          </p>
        </div>
        <div className="familiar_step_list__header_item" >
          <p>
            <strong>
              Parentesco
            </strong>
          </p> 
        </div>
        <div className="familiar_step_list__header_item" >
          <p>
            <strong>
              Estudio
            </strong>
          </p>
        </div>
        <div className="familiar_step_list__header_item" >
          <p>
            <strong>
              Ocupación
            </strong>
          </p>
        </div>
        <div className="familiar_step_list__header_item" >
          <p>
            <strong>
              Depende económicamente de usted
            </strong>
          </p>
          
        </div>
        
    </div>
  )
}

export default ListHeaderMovile
