import React, {useState} from 'react'

/*Icons */
import IosTrashOutline from 'react-ionicons/lib/IosTrashOutline'
import MdCreate from 'react-ionicons/lib/MdCreate'

/*Components */
import ModalContainerDelete from '../../../../../../Components/Modals/ModalContainerDelete'

function ListContent(props) {
  const [show, setShow] = useState(false)
  
  const { 
    id,
    language, 
    lecture_level, 
    level, 
    listening_level, 
    speaking_level ,
  } = props.language
  // startDeleteLanguage
  const {removeItem, setEditLanguage, index,} = props
  const [hover, setHover] = useState(false)


  return (
    <div 
    onMouseEnter={() => setHover(true)} 
    onMouseLeave={() =>setHover(false)}
    className={
      'fade-in '+
      (hover ? 'language_step_list__container hover_list': 'language_step_list__container ') 
      }>
        <div className="language_step_list__item" > 
          <span className="header-list">
            <strong>
              Idioma:
            </strong>
          </span>
          <span>
            {language.toUpperCase()}
          </span>
        </div>
        <div className="language_step_list__item" > 
          <span className="header-list">
            <strong>
              Habla:
            </strong>
          </span>
          <span>
            {speaking_level.toUpperCase() == 'BASIC' && 'BASICO'}
            {speaking_level.toUpperCase() == 'INTERMEDIATE' && 'INTERMEDIO'}
            {speaking_level.toUpperCase() == 'ADVANCE' && 'AVANZADO'}

          </span>
        </div>
        <div className="language_step_list__item" >
          <span className="header-list">
            <strong>
              Escucha:
            </strong>
          </span>
          <span>
            {listening_level.toUpperCase() == 'BASIC' && 'BASICO'}
            {listening_level.toUpperCase() == 'INTERMEDIATE' && 'INTERMEDIO'}
            {listening_level.toUpperCase() == 'ADVANCE' && 'AVANZADO'}
          </span> 
        </div>
        <div className="language_step_list__item" >
          <span className="header-list">
            <strong>
              Lectura:
            </strong>
          </span>
          <span>
            {lecture_level.toUpperCase() == 'BASIC' && 'BASICO'}
            {lecture_level.toUpperCase() == 'INTERMEDIATE' && 'INTERMEDIO'}
            {lecture_level.toUpperCase() == 'ADVANCE' && 'AVANZADO'}

          </span> 
        </div>
        <div className="language_step_list__actions" >
        <div>
            <IosTrashOutline
            // onClick={ () => removeItem(id, index)} 
            onClick={ () => setShow(true) }
            fontSize="45px" color='#dc3545'   
            className='circle-icon' />
            <ModalContainerDelete
            // onClick={ () => removeItem(id, index)} 
            identification={id}
            index={index}   
            show={show}
            setShow={setShow}
            removeItem={removeItem}
            title={'Eliminar Idioma'}
            message={'Realmente desea eliminar este dato?'}
            />
          </div>

          <MdCreate
          onClick={ () => setEditLanguage(index)} 
          fontSize="45px" color='#dc3545'   
          className='circle-icon' />
        </div>
    </div>
  )
}

export default ListContent
