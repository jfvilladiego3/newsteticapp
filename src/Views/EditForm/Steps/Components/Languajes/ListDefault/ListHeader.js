import React from 'react'


function ListHeader() {
  return (
    <div className='language_step_list__headers fade-in header-content'>
        <div className="language_step_list__header_item" > 
          <p>
            <strong>
              Idioma
            </strong>
          </p>
        </div>
        <div className="language_step_list__header_item" > 
          <p>
            <strong>
              Habla
            </strong>
          </p>
        </div>
        <div className="language_step_list__header_item" >
          <p>
            <strong>
              Escucha
            </strong>
          </p> 
        </div>
        <div className="language_step_list__header_item" >
          <p>
            <strong>
              Lectura
            </strong>
          </p>
        </div>
        
    </div>
  )
}

export default ListHeader
