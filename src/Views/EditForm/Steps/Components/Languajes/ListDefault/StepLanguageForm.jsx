import React, { useState, useContext, useEffect } from "react";

/*Context */
import { EditFormContext } from "../../../../EditFormContext";

/* Formik */
import { useFormik } from "formik";
import ErrorField from "../../../../../../Components/Form/ErrorField";
import ValidationSchema from '../../../Validations/ValidationStepLanguage'

/*Icons */
import IosTrashOutline from 'react-ionicons/lib/IosTrashOutline'
import IosClose from 'react-ionicons/lib/IosClose'
import IosCheckmark from "react-ionicons/lib/IosCheckmark";

function StepLanguageForm(props) {
  const context = useContext(EditFormContext);
  const { state, actions } = context;
  const { listLanguages, user } = state;
  const { startCreateLanguage, startEditLanguage } = actions;
  const {
    id,
    language,
    lecture_level,
    level,
    listening_level,
    speaking_level
  } = props.language;

  const {  setEditLanguage, index } = props;
  const [hover, setHover] = useState(false);

  const formik = useFormik({
    validationSchema: ValidationSchema,
    initialValues: {
      id: id ? id : null,
      fk_user_id: user.fk_user_id,
      language: language ? language.toUpperCase() : "",
      speaking_level : speaking_level ? speaking_level.toUpperCase() : "",
      lecture_level: lecture_level ? lecture_level.toUpperCase() : "",
      listening_level: listening_level ? listening_level.toUpperCase() : ""
    },
    onSubmit: values => {
      if(id && id != undefined && id != null){
        startEditLanguage( id, values, index )
        setEditLanguage(null)
      }else{
        startCreateLanguage(values)
        setEditLanguage(null)
      }
    }
  });

  const {
    values,
    errors,
    touched,
    isValid,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
    setFieldValue
  } = formik;

  return (
    <form
      onSubmit={formik.handleSubmit}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      className={
        'fade-in '+
        (hover
          ? "language_step_list__container hover_list"
          : "language_step_list__container ")
      }
    >
      <div className="language_step_list__item fade-in">
        <label htmlFor="" className="header-list">Idioma</label>
        <select
          className={
            touched.language && errors.language
              ? "form-control has-error"
              : "form-control"
          }
          name="language"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.language}
        >
          <option></option>
          {listLanguages
            .filter(lan => {
              return lan.nombre != "" && lan.nombre != null;
            })
            .map((lan, i) => (
              <option key={i} value={lan.nombre.toUpperCase()}>
                {lan.nombre.toUpperCase()}
              </option>
            ))}
        </select>
        <ErrorField
            touched={touched.language}
            message={errors.language}
          />
      </div>
      <div className="language_step_list__item">
        <label htmlFor="" className="header-list">Habla</label>
        <span>
          <select
            className={
              touched.speaking_level && errors.speaking_level
                ? "form-control has-error"
                : "form-control"
            }
            name="speaking_level"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.speaking_level}
          >
            <option></option>
            <option value="BASIC">BASICO</option>
            <option value="INTERMEDIATE">INTERMEDIO</option>
            <option value="ADVANCE">AVANZADO</option>
          </select>
          <ErrorField
            touched={touched.speaking_level}
            message={errors.speaking_level}
          />
        </span>
      </div>
      <div className="language_step_list__item">
        <label htmlFor="" className="header-list">Escucha</label>
        <span>
          <select
            className={
              touched.listening_level && errors.listening_level
                ? "form-control has-error"
                : "form-control"
            }
            name="listening_level"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.listening_level}
          >
            <option></option>
            <option value="BASIC">BASICO</option>
            <option value="INTERMEDIATE">INTERMEDIO</option>
            <option value="ADVANCE">AVANZADO</option>
          </select>
          <ErrorField
            touched={touched.listening_level}
            message={errors.listening_level}
          />
        </span>
      </div>
      <div className="language_step_list__item">
        <label htmlFor="" className="header-list">Lectura</label>
        <span>
          <select
            className={
              touched.lecture_level && errors.lecture_level
                ? "form-control has-error"
                : "form-control"
            }
            name="lecture_level"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.lecture_level}
          >
            <option></option>
            <option value="BASIC">BASICO</option>
            <option value="INTERMEDIATE">INTERMEDIO</option>
            <option value="ADVANCE">AVANZADO</option>
          </select>
          <ErrorField
            touched={touched.lecture_level}
            message={errors.lecture_level}
          />
        </span>
      </div>
      <div className="language_step_list__actions">
        <button 
        type="submit" 
        className="btn btn-success circle-button btn-shadow">
          <IosCheckmark
          fontSize="40px"
          color="white"
          className='circle-btn-icon'
        />
        </button>
        {/* <IosCheckmark
          onClick={() => setEditFamiliar(false)}
          fontSize="45px"
          color="white"
          className="circle-icon success"
        /> */}
        <IosClose
          onClick={() =>  setEditLanguage(null)}
          fontSize="45px"
          color="white"
          className="circle-icon danger"
        />
      </div>
    </form>
  );
}

export default StepLanguageForm;
