import React, {useContext, useState, useEffect, useRef} from 'react'
/*Formik and yup */
import { Formik, FieldArray, Field  } from 'formik';
/* Validations */
import ValidationSchema from '../../../Steps/Validations/ValidationStepLanguage'
/*Context */
import {EditFormContext} from '../../../EditFormContext'

/*Components */
import ErrorField from "../../../../../Components/ErrorsField";

function StepLanguageForm() {
    const { 
        user, 
        languages, 
        listLanguages, 
        setLanguages,
        saveLanguage, 
    } = useContext(EditFormContext) 

    return (
        <Formik
        initialValues={{
            language: "",
            lecture_level: '',
            listening_level:'',
            speaking_level: '',
        }}
        validationSchema={ValidationSchema}
        onSubmit={(values, { setSubmitting, resetForm }) => {
            saveLanguage( {
                fk_user_id: user.fk_user_id,
                language: values.language,
                lecture_level: values.lecture_level,
                listening_level: values.listening_level,
                speaking_level: values.speaking_level,
            }).then(data=>{
                setLanguages([
                    ...languages,
                    {
                        id: data.language.id,
                        language: values.language,
                        lecture_level: values.lecture_level,
                        listening_level: values.listening_level,
                        speaking_level: values.speaking_level,
                    }
                ])
                resetForm({})
            })
        }}
    >
        {({
        values,
        errors,
        touched,
        isValid,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        setFieldValue
        }) => (
                    <form onSubmit={handleSubmit} className="row familiars_form">
                        
                        <div className="familiars_form__container_title col-12" > 
                            <h5 className="familiars_form__title" > 
                                Idiomas
                            </h5>
                        </div>
                        

                        <div className="col-xs-12 col-md-6">
                            <label htmlFor="">Idioma</label>
                            <select 
                            className={touched.language && errors.language ? "form-control has-error" : 'form-control'} 
                            name="language"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.language}>
                                <option></option>
                                {listLanguages
                                .filter(lan => { return lan.nombre != '' && lan.nombre != null })
                                .map((lan, i) => 
                                        <option key={i} value={lan.nombre}>{lan.nombre}</option>
                                )}
                            </select>
                            <ErrorField touched={touched.language} message={errors.language} />
                        </div>
                        
                        <div className="col-xs-12 col-md-6">
                            <label htmlFor="">Habla</label>
                            <select 
                            className={touched.speaking_level && errors.speaking_level ? "form-control has-error" : 'form-control'} 
                            name="speaking_level"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.speaking_level}>
                                <option></option>BASICO
                                <option value='BASIC'></option>
                                <option value='INTERMEDIATE'>INTERMEDIO</option>
                                <option value='ADVANCE'>AVANZADO</option>
                            </select>
                            <ErrorField touched={touched.speaking_level} message={errors.speaking_level} />
                        </div>

                        <div className="col-xs-12 col-md-6">
                            <label htmlFor="">Escucha</label>
                            <select 
                            className={touched.listening_level && errors.listening_level ? "form-control has-error" : 'form-control'} 
                            name="listening_level"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.listening_level}>
                                <option></option>
                                <option value='BASIC'>BASICO</option>
                                <option value='INTERMEDIATE'>INTERMEDIO</option>
                                <option value='ADVANCE'>AVANZADO</option>
                            </select>
                            <ErrorField touched={touched.listening_level} message={errors.listening_level} />
                        </div>


                        <div className="col-xs-12 col-md-6">
                            <label htmlFor="">Lectura</label>
                            <select 
                            className={touched.lecture_level && errors.lecture_level ? "form-control has-error" : 'form-control'} 
                            name="lecture_level"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.lecture_level}>
                                <option></option>
                                <option value='BASIC'>BASICO</option>
                                <option value='INTERMEDIATE'>INTERMEDIO</option>
                                <option value='ADVANCE'>AVANZADO</option>
                            </select>
                            <ErrorField touched={touched.lecture_level} message={errors.lecture_level} />
                        </div>

                       

                        <div className="col-12">
                            
                            <div className="">
                                <button className="btn btn-success"
                                type='submit'
                                >
                                    Guardar
                                </button>
                            </div>
                        </div>

                    </form>
    )}
    </Formik>
    )
}

export default StepLanguageForm
