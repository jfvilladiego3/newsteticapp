import React, {useContext, useEffect, useState} from 'react'
/*Context */
import {EditFormContext} from '../../../EditFormContext'
/*Components */
import ListHeader from './ListDefault/ListHeader';
import ListContent from './ListDefault/ListContent';
import StepLanguageForm from './ListDefault/StepLanguageForm';

import MdAdd from 'react-ionicons/lib/MdAdd'

function StepLanguagesList() {
  const [editLanguage, setEditLanguage] = useState(null)
  const [createLanguage, setCreateLanguage] = useState(null)
  const context = useContext(EditFormContext) 
  const { state, actions } = context 
  const { languages } = state 
  const { startDeleteLanguage   } = actions 


  return (
    <div className="language_step_list" > 
      <div className=" item-right" > 
        <button 
        onClick={ () => setCreateLanguage(languages.length) }
        type="button" 
        className="btn btn-warning ">
          Agregar
          <MdAdd fontSize="20px" color="black" />
        </button>
      </div>
        <ListHeader/>
        {languages.map((language, i) => (
          <div key={i}>
            {editLanguage != i &&  <ListContent 
            index={i}
            language={language}
            removeItem={startDeleteLanguage}
            setEditLanguage={setEditLanguage}
            />}
            {editLanguage == i && <StepLanguageForm 
            index={i}
            language={language}
            removeItem={startDeleteLanguage}
            setEditLanguage={setEditLanguage}
            />}
          </div>
        ))}

        {createLanguage  == languages.length && <StepLanguageForm 
          index={languages.length}
          language={{}}
          setEditLanguage={setCreateLanguage}
          />}
    </div>
  )
}

export default StepLanguagesList
