import React, {useContext} from 'react'
import {EditFormContext} from '../../../EditFormContext'
/* Components */
import ListHeader from './ListDefault/ListHeader'
import ListContent from './ListDefault/ListContent'

function EducationLevelsList({formik}) {
  const context = useContext(EditFormContext)
  const {state, actions} = context
  const {user, educations} = state

  return (
    <div>
      <ListHeader/>
      <ListContent formik={formik} />
    </div>
  )
}

export default EducationLevelsList
