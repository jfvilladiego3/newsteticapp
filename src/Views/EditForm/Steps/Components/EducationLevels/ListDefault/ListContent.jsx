import React from "react";

import { toInputUppercase } from "../../../../../../Utils/inputUppercase";

// import ValidationSchema from "./Validations/ValidationStepPersonailInformation";
import ErrorField from "../../../../../../Components/Form/ErrorField";

function ListContent({ formik }) {
  const {
    values,
    errors,
    touched,
    isValid,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
    setFieldValue,
  } = formik;

  return (
    <div className="education_levels_step_list__content">
      <div className="education_levels_step_list__item">
        Primaria (último año aprobado) (*)
      </div>
      <div className="education_levels_step_list__item">
        <div className="">
          <select
            className={
              touched.primary_title && errors.primary_title
                ? "form-control has-error"
                : "form-control"
            }
            name={`primary_title`}
            onChange={handleChange}
            onBlur={handleBlur}
            type="text"
            value={values.primary_title}
          >
            <option></option>
            {["1", "2", "3", "4", "5"].map((grade, i) => (
              <option key={i} value={grade}>
                {grade}
              </option>
            ))}
          </select>
          <ErrorField
            touched={touched.primary_title}
            message={errors.primary_title}
          />
        </div>
      </div>

      <div className="education_levels_step_list__item">Bachillerato (*)</div>
      <div className="education_levels_step_list__item">
        <div className="">
          <select
            className={
              touched.secondary_title && errors.secondary_title
                ? "form-control has-error"
                : "form-control"
            }
            name={`secondary_title`}
            onChange={handleChange}
            onBlur={handleBlur}
            type="text"
            value={values.secondary_title}
          >
            <option></option>
            {["6", "7", "8", "9", "10", "11", "12"].map((grade, i) => (
              <option key={i} value={grade}>
                {grade}
              </option>
            ))}
          </select>
          <ErrorField
            touched={touched.secondary_title}
            message={errors.secondary_title}
          />
        </div>
      </div>

      <div className="education_levels_step_list__item">Técnico (*)</div>
      <div
        className={
          values.technical_study == 1
            ? "education_levels_step_list__item_confirmation"
            : "education_levels_step_list__item"
        }
      >
        <div className="">
          <select
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.technical_study}
            name="technical_study"
            className="form-control"
          >
            <option value={0}>NO</option>
            <option value={1}>SI</option>
          </select>
        </div>
        {values.technical_study == 1 && (
          <div className="">
            <input
              className={
                touched.technical_title && errors.technical_title
                  ? "form-control has-error"
                  : "form-control"
              }
              name={`technical_title`}
              onChange={handleChange}
              onBlur={handleBlur}
              type="text"
              value={values.technical_title}
              onInput={toInputUppercase}
            />
            <ErrorField
              touched={touched.technical_title}
              message={errors.technical_title}
            />
          </div>
        )}
      </div>

      <div className="education_levels_step_list__item">Tecnológico (*)</div>
      <div
        className={
          values.technology_study == 1
            ? "education_levels_step_list__item_confirmation"
            : "education_levels_step_list__item"
        }
      >
        <div className="confirmation">
          <select
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.technology_study}
            name="technology_study"
            className="form-control"
          >
            <option value={0}>NO</option>
            <option value={1}>SI</option>
          </select>
        </div>
        {values.technology_study == 1 && (
          <div className="">
            <input
              className={
                touched.technology_title && errors.technology_title
                  ? "form-control has-error"
                  : "form-control"
              }
              name={`technology_title`}
              onChange={handleChange}
              onBlur={handleBlur}
              type="text"
              value={values.technology_title}
              onInput={toInputUppercase}
            />
            <ErrorField
              touched={touched.technology_title}
              message={errors.technology_title}
            />
          </div>
        )}
      </div>

      <div className="education_levels_step_list__item">Universitario (*)</div>
      <div
        className={
          values.university_study == 1
            ? "education_levels_step_list__item_confirmation"
            : "education_levels_step_list__item"
        }
      >
        <div className="confirmation">
          <select
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.university_study}
            name="university_study"
            className="form-control"
          >
            <option value={0}>NO</option>
            <option value={1}>SI</option>
          </select>
        </div>
        {values.university_study == 1 && (
          <div className="">
            <input
              className={
                touched.university_title && errors.university_title
                  ? "form-control has-error"
                  : "form-control"
              }
              name={`university_title`}
              onChange={handleChange}
              onBlur={handleBlur}
              type="text"
              value={values.university_title}
              onInput={toInputUppercase}
            />
            <ErrorField
              touched={touched.university_title}
              message={errors.university_title}
            />
          </div>
        )}
      </div>

      <div className="education_levels_step_list__item">Especialización (*)</div>
      <div
        className={
          values.specialitation_study == 1
            ? "education_levels_step_list__item_confirmation"
            : "education_levels_step_list__item"
        }
      >
        <div className="confirmation">
          <select
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.specialitation_study}
            name="specialitation_study"
            className="form-control"
          >
            <option value={0}>NO</option>
            <option value={1}>SI</option>
          </select>
        </div>
        {values.specialitation_study == 1 && (
          <div className="">
            <input
              className={
                touched.specialitation_title && errors.specialitation_title
                  ? "form-control has-error"
                  : "form-control"
              }
              name={`specialitation_title`}
              onChange={handleChange}
              onBlur={handleBlur}
              type="text"
              value={values.specialitation_title}
              onInput={toInputUppercase}
            />
            <ErrorField
              touched={touched.specialitation_title}
              message={errors.specialitation_title}
            />
          </div>
        )}
      </div>

      <div className="education_levels_step_list__item">Maestría (*)</div>
      <div
        className={
          values.master_study == 1
            ? "education_levels_step_list__item_confirmation"
            : "education_levels_step_list__item"
        }
      >
        <div className="confirmation">
          <select
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.master_study}
            name="master_study"
            className="form-control"
          >
            <option value={0}>NO</option>
            <option value={1}>SI</option>
          </select>
        </div>
        {values.master_study == 1 && (
          <div className="">
            <input
              className={
                touched.master_title && errors.master_title
                  ? "form-control has-error"
                  : "form-control"
              }
              name={`master_title`}
              onChange={handleChange}
              onBlur={handleBlur}
              type="text"
              value={values.master_title}
              onInput={toInputUppercase}
            />
            <ErrorField
              touched={touched.master_title}
              message={errors.master_title}
            />
          </div>
        )}
      </div>
      <div className="education_levels_step_list__item">Doctorado (*)</div>
      <div
        className={
          values.doctorate_study == 1
            ? "education_levels_step_list__item_confirmation"
            : "education_levels_step_list__item"
        }
      >
        <div className="confirmation">
          <select
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.doctorate_study}
            name="doctorate_study"
            className="form-control"
          >
            <option value={0}>NO</option>
            <option value={1}>SI</option>
          </select>
        </div>
        {values.doctorate_study == 1 && (
          <div className="">
            <input
              className={
                touched.doctorate_title && errors.doctorate_title
                  ? "form-control has-error"
                  : "form-control"
              }
              name={`doctorate_title`}
              onChange={handleChange}
              onBlur={handleBlur}
              type="text"
              value={values.doctorate_title}
              onInput={toInputUppercase}
            />
            <ErrorField
              touched={touched.doctorate_title}
              message={errors.doctorate_title}
            />
          </div>
        )}
      </div>
      <div className="education_levels_step_list__item_container">
        <label htmlFor="">
          ¿Actualmente se encuentra realizando algún tipo de estudio? (*)
        </label>
        <div className="confirmation">
          <select
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.currently_study}
            name="currently_study"
            className="form-control"
          >
            <option value={0}>NO</option>
            <option value={1}>SI</option>
          </select>
        </div>
      </div>
      <div className="education_levels_step_list__item_currently">
        {values.currently_study == 1 && (
          <div className="">
            <label htmlFor="">¿Cuál? (*)</label>
            <input
              className={
                touched.currently_taken && errors.currently_taken
                  ? "form-control has-error"
                  : "form-control"
              }
              name={`currently_taken`}
              onChange={handleChange}
              onBlur={handleBlur}
              type="text"
              value={values.currently_taken}
              onInput={toInputUppercase}
            />
            <ErrorField
              touched={touched.currently_taken}
              message={errors.currently_taken}
            />
          </div>
        )}
        {values.currently_study == 1 && (
          <div className="">
            <label htmlFor="">¿Grado o semestre? (*)</label>
            <input
              className={
                touched.currently_grade && errors.currently_grade
                  ? "form-control has-error"
                  : "form-control"
              }
              name={`currently_grade`}
              onChange={handleChange}
              onBlur={handleBlur}
              type="text"
              value={values.currently_grade}
              onInput={toInputUppercase}
            />
            <ErrorField
              touched={touched.currently_grade}
              message={errors.currently_grade}
            />
          </div>
        )}
        {values.currently_study == 1 && (
          <div className="">
            <label htmlFor="">En que institución o universidad (*)</label>
            <input
              className={
                touched.currently_institution && errors.currently_institution
                  ? "form-control has-error"
                  : "form-control"
              }
              name={`currently_institution`}
              onChange={handleChange}
              onBlur={handleBlur}
              type="text"
              value={values.currently_institution}
              onInput={toInputUppercase}
            />
            <ErrorField
              touched={touched.currently_institution}
              message={errors.currently_institution}
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default ListContent;
