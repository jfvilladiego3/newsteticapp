import React from 'react'

function ListHeader() {
  return (
    <div className='education_levels_step_list__headers fade-in'>
        <div className="education_levels_step_list__header_item" > 
          <p>
            <strong>
              Tipo de estudio
            </strong>
          </p>
        </div>
        <div className="education_levels_step_list__header_item" > 
          <p>
            <strong>
              Título obtenido
            </strong>
          </p>
        </div>
    </div>
  )
}

export default ListHeader
