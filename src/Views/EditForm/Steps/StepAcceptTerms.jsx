import React, {useContext} from 'react'
/*Formik and yup */
import { Formik  } from 'formik';
/*Context */
import {EditFormContext} from '../EditFormContext'

/*Components */
import ButtonNextStep from "../../../Components/ButtonNextStep";

function StepAcceptTerms() {
    const context = useContext(EditFormContext)
    const state = context.state
    const actions = context.actions
    const { main, user, setUser, updateUser  } = state
    const { startUserAcceptTerms  } = actions
    return (
        <Formik
        initialValues={{
          accept_terms: user ? user.accept_terms : '',
        }}
        //validationSchema={ValidationSchema}
        onSubmit={(values, { setSubmitting, resetForm }) => {
          startUserAcceptTerms(user)
        }}
    >
        {({
        values,
        errors,
        touched,
        isValid,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        setFieldValue
        }) => (
                    <form onSubmit={handleSubmit} className="container">
                        <div className="col-12">
                          <p className="accept_terms_text">
                            Yo, <strong>{user.complete_name}</strong>, identificado(a) con cédula de ciudadanía No. 
                            <strong>{user.id_card}{" "} {user.id_card_expedition_place}</strong> , en calidad de titular de mis datos personales y 
                            de acuerdo con la Ley estatutaria 1581 de 2012 de protección de datos, normas reglamentarias y 
                            demás normas concordantes, autorizo a NEW STETIC. S.A. para que los datos registrados en este 
                            formulario, sean incorporados en su base de datos con la finalidad de realizar gestión 
                            administrativa, de seguridad social, informes estadísticos, indicadores de gestión, invitación 
                            a eventos y el uso pertinente en los medios de comunicación de la Organización. Así mismo autorizo
                            para que mis datos biométricos como huellas e imágenes registradas en fotografías y videos sean 
                            incorporadas en una base de datos bajo responsabilidad de NEW STETIC S.A. con la finalidad de que 
                            se lleve a cabo el proceso de identificación dactilar o se comparta la tarjeta con mis impresiones 
                            dactilares a las autoridades competentes en caso de que se requiera. Además, de acuerdo con los 
                            Estándares de Seguridad BASC, autorizo a NEW STETIC. S.A. a realizar pruebas que permitan detectar 
                            el consumo de alcohol, drogas y otras adicciones antes de la contratación, cuando haya sospecha 
                            justificable y aleatoriamente cuando se considere necesario. Adicional a esto, como representante 
                            legal de los menores relacionados en el presente formato, autorizo para que sus datos sean
                            incorporados a una base de datos bajo responsabilidad de NEW STETIC S.A., para que sean tratados 
                            con la finalidad de realizar gestión de administrativa, suministrar información para la afiliación 
                            seguridad social, caja de compensación, invitación a eventos y el uso pertinente en los medios de 
                            comunicación de la Organización. De igual modo, declaro haber sido informado de que puedo 
                            ejercitar mis derechos de acceso, corrección, supresión, revocación o reclamo por infracción 
                            sobre mis datos, mediante escrito dirigido a NEW STETIC S.A. a la dirección de correo electrónico 
                            protecciondatos@newstetic.com, o a través de correo ordinario,indicando en el asunto el derecho que 
                            desea ejercitar. NEW STETIC S.A, en acuerdo con lo que establece la Ley Estatutaria 1581 de 2012, le 
                            informa que en la recolección, almacenamiento y tratamiento, sus datos serán tratados conforme al 
                            ordenamiento legal vigente que rige la protección de datos personales, garantizando el ejercicio de 
                            sus derechos como titular.
                          </p>

                            
                        </div>
                        <div className="col-12 container_check_accept_terms">
                          <div className="form-check form-check-inline ">
                              <input 
                              className="form-check-input" 
                              name="accept_terms"
                              type="checkbox" 
                              id="accept_terms"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={1}
                              checked={ values.accept_terms == 1 ? true : false}
                              />
                              <label className="form-check-label"  >Aceptar</label>
                            </div>
                        </div>
                        

                        {values.accept_terms &&  <div className="col-12">
                            <br></br>
                            <div className="container_btns_step">
                                <ButtonNextStep
                                type='submit'
                                />
                            </div>
                        </div>}

                    </form>
    )}
    </Formik>
    )
}

export default StepAcceptTerms
