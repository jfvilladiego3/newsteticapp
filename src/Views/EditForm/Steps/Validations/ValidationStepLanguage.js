import * as Yup from "yup";

const ValidationSchema = Yup.object().shape({
  language: Yup.string()
    .required("Este campo es requerido"),
  speaking_level: Yup.string()
    .required("Este campo es requerido"),
  lecture_level: Yup.string()
    .required("Este campo es requerido"),
  listening_level: Yup.string()
    .required("Este campo es requerido"),
});

export default ValidationSchema;