import * as Yup from "yup";

const ValidationSchema = Yup.object().shape({
  technical_title: Yup.string()
    .when('technical_study', {
      is: '1',
      then: Yup.string().required("Este campo es requerido")
    }),
  technology_title: Yup.string()
    .when('technology_study', {
      is: '1',
      then: Yup.string().required("Este campo es requerido")
    }),
  university_title: Yup.string()
    .when('university_study', {
      is: '1',
      then: Yup.string().required("Este campo es requerido")
    }),
  specialitation_title: Yup.string()
    .when('specialitation_study', {
      is: '1',
      then: Yup.string().required("Este campo es requerido")
    }),
  master_title: Yup.string()
    .when('master_study', {
      is: '1',
      then: Yup.string().required("Este campo es requerido")
    }),
  doctorate_title: Yup.string()
    .when('doctorate_study', {
      is: '1',
      then: Yup.string().required("Este campo es requerido")
    }),
  currently_study: Yup.string()
    .when('doctorate_study', {
      is: '1',
      then: Yup.string().required("Este campo es requerido")
    }),
  currently_grade: Yup.string()
    .when('currently_study', {
      is: '1',
      then: Yup.string().required("Este campo es requerido")
    }),
  currently_institution: Yup.string()
    .when('currently_study', {
      is: '1',
      then: Yup.string().required("Este campo es requerido")
    }),
  currently_taken: Yup.string()
    .when('currently_study', {
      is: '1',
      then: Yup.string().required("Este campo es requerido")
    }),
});

export default ValidationSchema;