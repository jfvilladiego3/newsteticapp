import * as Yup from "yup";

const ValidationSchema = Yup.object().shape({
  complete_name: Yup.string()
    .required("Este campo es requerido"),
  age: Yup.number()
    .required("Este campo es requerido y debe ser numérico"),
  relationship: Yup.string()
    .required("Este campo es requerido"),
  occupation: Yup.string()
    .required("Este campo es requerido"),
  economic_dependency: Yup.string()
    .required("Este campo es requerido"),
  education_level: Yup.string()
    .required("Este campo es requerido"),
  is_roomie: Yup.string()
  .required("Este campo es requerido")
});

export default ValidationSchema;
