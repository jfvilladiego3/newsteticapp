 
import * as Yup from "yup";

const ValidationSchema = Yup.object().shape({
  gender: Yup.string()
    .required("Este campo es requerido"),
  id_card_rh: Yup.string()
    .required("Este campo es requerido"),
  id_card_expedition_place: Yup.string()
    .required("Este campo es requerido").nullable(),
  civil_status: Yup.string()
    .required("Este campo es requerido").nullable(),
  type_employee: Yup.string()
    .required("Este campo es requerido").nullable(),
  birth_date: Yup.string()
    .required("Este campo es requerido").nullable(),
  address: Yup.string()
    .required("Este campo es requerido"),
  neighborhood: Yup.string()
    .required("Este campo es requerido"),
  have_sons: Yup.string()
    .required("Este campo es requerido"),
  child_number: Yup.string()
    .when('have_sons', {
        is: '1',
        then: Yup.string().required("Este campo es requerido")
      }),
  residence_department: Yup.string()
    .required("Este campo es requerido"),
  city: Yup.string()
    .required("Este campo es requerido"),
  email: Yup.string()
    .required("Este campo es requerido")
    .email('Este campo debe de ser un email'),
  private_transport: Yup.string()
    .required("Este campo es requerido"),
  phone: Yup.string()
    .required("Este campo es requerido"),
  home_type: Yup.string()
    .required("Este campo es requerido"),
  smoker: Yup.string()
    .required("Este campo es requerido"),
  alcoholic: Yup.string()
    .required("Este campo es requerido"),
});


export default ValidationSchema;
