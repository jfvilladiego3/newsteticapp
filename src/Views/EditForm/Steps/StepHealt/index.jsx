import React, { useState, useContext, useEffect } from "react";
import MultiSelect from "react-multi-select-component";

import { useStepHealt } from "./useStepHealt";

import {
  toInputUppercaseWhitNumbersAndSpace
} from "../../../../Utils/inputUppercase";

/*Components */
import ButtonNextStep from "../../../../Components/ButtonNextStep";
import ButtonPrevStep from "../../../../Components/ButtonPrevStep";
import ErrorField from "../../../../Components/Form/ErrorField";

function StepHealt() {
  const {
    options,
    startPrevStep,
    selected,
    sendDiseases,
    setSelectedChange,
    other,
    otherName,
    setOtherName,
    showOtherValidation
  } = useStepHealt();

  return (
    <form onSubmit={(e) => sendDiseases(e)}>
      <div className="">
        <div className="center-text">
          <h5>¿Cuál de las siguientes enfermedades sufre?</h5>
        </div>
      </div>

      <div>
        <MultiSelect
          hasSelectAll={false}
          disableSearch={true}
          options={options}
          value={selected}
          onChange={(e) => setSelectedChange(e)}
        />
      </div>

      <br />

      {other && (
        <div>
          <label htmlFor="">¿Cuál? (*)</label>
          <input 
            type="text" 
            className="form-control" 
            name="otherName" 
            value={otherName}
            onInput={toInputUppercaseWhitNumbersAndSpace}
            onChange={(event) => setOtherName(event.target.value) }/>
          {showOtherValidation && <ErrorField touched={true} message={"Este campo es requerido"} />}
        </div>
      )}

      <div className="col-12">
        <br></br>
        <div className="container_btns_step">
          <ButtonPrevStep type="button" mainPrev={startPrevStep} />
          <ButtonNextStep type="submit" />
        </div>
      </div>
    </form>
  );
}

export default StepHealt;
