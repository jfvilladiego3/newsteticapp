import { useState, useContext, useEffect } from "react";

/*Context */
import { EditFormContext } from "../../../EditFormContext";

export const useStepHealt = () => {
  const context = useContext(EditFormContext);
  const state = context.state;
  const actions = context.actions;
  const { user, diseases, userDiseases } = state;
  const { startPrevStep } = actions;
  const [selected, setSelected] = useState([]);
  const [options, setOptions] = useState([]);
  const [other, setOther] = useState(false)
  const [otherName, setOtherName] = useState('')
  const [showOtherValidation, setShowOtherValidation] = useState(false)

  const chargeDataOptions = () => {
    let dataOptions = [];

    if (diseases) {
      dataOptions.push({
        label: "NO POSEO NINGUNA",
        value: 0,
      });
      diseases.map((disease) => {
        dataOptions.push({
          label: disease.name,
          value: disease.id,
        });
      });
      setOptions(dataOptions);
    }
  }

  const userIsNotDiseases = () => {
    if (userDiseases && user.is_disease == 0) {
      setOther(false)
      setOtherName('')

      setSelected([
        {
          label: "NO POSEO NINGUNA",
          value: 0,
        },
      ]);
    }
  }

  const userIsDiseases = () => {
    if (userDiseases && user?.is_disease == 1) {
      let valueSelected = [];

      userDiseases.map((disease) => {
        const newDis = diseases.find(dis => dis.id == disease.diseases_id);

        if (newDis) {
          valueSelected.push({
            label: newDis.name,
            value: newDis.id
          });

          if (newDis.name === 'OTRA') {
            setOther(true)
            setOtherName(disease.other_name)
          }
        }
      });

      setSelected(valueSelected);
    }
  }

  useEffect(() => {
    chargeDataOptions()
    userIsNotDiseases()
    userIsDiseases()
  }, [state]);


  const setSelectedChange = (selection) => {
    let isHealt = false;
    selection.map((select, i) => {
      if (select.value === 0 && i >= 0) {
        return (isHealt = true);
      } else {
        return (isHealt = false);
      }
    });

    if (isOther(selection)) {
      setOther(true)
    } else {
      setOther(false)
      setOtherName('')
    }

    if (isHealt) {
      setOther(false)
      setOtherName('')

      setSelected([
        {
          label: "NO POSEO NINGUNA",
          value: 0,
        },
      ]);
    } else {
      if (selection[0].value == 0) {
        selection.splice(0, 1);
      }
      setSelected(selection);
    }
  };

  const otherDisease = () => {
    return diseases.find(disease => disease.name == 'OTRA')
  }

  const sendDiseases = (e) => {
    e.preventDefault();

    if (diseases) {
      const otherDiseaseId = otherDisease()?.id

      if (selected && selected.length !== 0) {
        const is_disease = selected[0].value === 0 ? 0 : 1;
        let data = [];
        let breackSave = false

        selected.map((disease) => {
          let other_name_value = ''
          if (disease.value == otherDiseaseId) {
            if (otherName === '' || otherName === null) {
              setShowOtherValidation(true)
              breackSave = true
            } else {
              other_name_value = otherName
            }
          }

          data.push({
            fk_user_id: user.fk_user_id,
            diseases_id: disease.value,
            other_name: other_name_value
          });
        });

        if (breackSave) {return}

        actions.startCreateDiseases(user.fk_user_id, is_disease, data, 4);
      }
    }
  };

  const isOther = (selectedOptions) => {
    return selectedOptions.find( opt => opt.label === 'OTRA' )
  }

  return {
    options,
    startPrevStep,
    selected,
    sendDiseases,
    setSelectedChange,
    other,
    otherName,
    setOtherName,
    showOtherValidation
  };
};
