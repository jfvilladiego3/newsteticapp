import React, {useContext} from 'react'

/* Formik */
import { useFormik } from "formik";
import ValidationSchema from './Validations/ValidationStepEducationLevels'
/*Context */
import { EditFormContext } from "../EditFormContext";
/*Components */
import ButtonNextStep from "../../../Components/ButtonNextStep";
import ButtonPrevStep from "../../../Components/ButtonPrevStep";
import EducationLevelsList from "./Components/EducationLevels/EducationLevelsList"
function StepEducationLevels() {
  const context = useContext(EditFormContext);
  const state = context.state;
  const actions = context.actions;
  const { user, educations } = state;
  const { startPrevStep, startCreateEducationLevels } = actions
  const formik = useFormik({
    validationSchema: ValidationSchema,
    initialValues: {
      fk_user_id: user?.fk_user_id ? user?.fk_user_id : '',

      primary: 'primary',
      primary_title: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'PRIMARY'){  return e.obtained_title }})
          .map( e => e.obtained_title ).toString().toUpperCase() : '',
      primary_study: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'PRIMARY'){  return e }})
          .map( e => e.study ).toString() : '',

      secondary: "SECONDARY",
      secondary_title: educations ? 
        educations
        .filter((e) => { if(e.education_level.toUpperCase() == 'SECONDARY'){  return e.obtained_title }})
        .map( e => e.obtained_title ).toString() : '',
      secondary_study: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'SECONDARY'){  return e.study }})
          .map( e => e.study ).toString().toUpperCase() : '',

      technical: "TECHNICAL",
      technical_title: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'TECHNICAL'){  return e }})
          .map( e => e.obtained_title ).toString() : '',
      technical_study: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'TECHNICAL'){  return e }})
          .map( e => e.study ).toString().toUpperCase() : '',

      technology: "TECHNOLOGY",
      technology_title: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'TECHNOLOGY'){  return e }})
          .map( e => e.obtained_title ).toString().toUpperCase() : '',
      technology_study: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'TECHNOLOGY'){  return e }})
          .map( e => e.study ).toString().toUpperCase() : '',

      university: "UNIVERSITY",
      university_title: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'UNIVERSITY'){  return e }})
          .map( e => e.obtained_title ).toString().toUpperCase() : '',
      university_study: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'UNIVERSITY'){  return e }})
          .map( e => e.study ).toString().toUpperCase() : '',

      specialitation: "SPECIALITATION",
      specialitation_title: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'SPECIALITATION'){  return e }})
          .map( e => {return e.obtained_title} ).toString().toUpperCase() : '',
      specialitation_study: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'SPECIALITATION'){  return e }})
          .map( e => e.study ).toString().toUpperCase() : '',

      master: "MASTER",
      master_title: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'MASTER'){  return e }})
          .map( e => {return e.obtained_title} ).toString().toUpperCase() : '',
      master_study: educations ? 
        educations
          .filter((e) => { if(e.education_level.toUpperCase() == 'MASTER'){  return e }})
          .map( e => e.study ).toString().toUpperCase() : '',

      doctorate: "DOCTORATE",
      doctorate_title: educations ? 
        educations
          .filter((e) => { if(e.education_level == 'DOCTORATE'){  return e }})
          .map( e => {return e.obtained_title} ).toString().toUpperCase() : '',
      doctorate_study: educations ? 
        educations
          .filter((e) => { if(e.education_level == 'DOCTORATE'){  return e }})
          .map( e => e.study ).toString().toUpperCase() : '',
      currently_study: user?.currently_grade != null  && user?.currently_grade != '' ? 1 : 0,
      currently_grade: user?.currently_grade ? user.currently_grade.toUpperCase() : '',
      currently_institution: user?.currently_institution ? user.currently_institution.toUpperCase() : '' ,
      currently_taken: user?.currently_taken ? user.currently_taken.toUpperCase() : ''
    },
    onSubmit: values => {
      const data = [
        {fk_user_id: values.fk_user_id, education_level: "PRIMARY", obtained_title: values.primary_title , study: values.primary_study},
        {fk_user_id: values.fk_user_id, education_level: "SECONDARY", obtained_title: values.secondary_title , study: values.secondary_study},
        {fk_user_id: values.fk_user_id, education_level: "TECHNICAL", obtained_title: values.technical_study == 1 ?  values.technical_title : '', study: values.technical_study},
        {fk_user_id: values.fk_user_id, education_level: "TECHNOLOGY", obtained_title: values.technology_study == 1 ?  values.technology_title : '', study: values.technology_study},
        {fk_user_id: values.fk_user_id, education_level: "UNIVERSITY", obtained_title: values.university_study == 1 ?  values.university_title : '', study: values.university_study},
        {fk_user_id: values.fk_user_id, education_level: "SPECIALITATION", obtained_title: values.specialitation_study == 1 ?  values.specialitation_title : '', study: values.specialitation_study},
        {fk_user_id: values.fk_user_id, education_level: "MASTER", obtained_title: values.master_study == 1 ?  values.master_title : '', study: values.master_study},
        {fk_user_id: values.fk_user_id, education_level: "DOCTORATE", obtained_title: values.doctorate_study == 1 ?  values.doctorate_title : '', study: values.doctorate_study},
      ]
      const updateUser = {
        currently_grade: values.currently_study == 1 ? values.currently_grade : '',
        currently_institution: values.currently_study == 1 ?  values.currently_institution : '',
        currently_taken: values.currently_study == 1 ?  values.currently_taken : ''
      }
      startCreateEducationLevels(user.fk_user_id, data, updateUser)
    }
  });

  const {
    values,
    errors,
    touched,
    isValid,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
    setFieldValue
  } = formik;
  
  return (
    <form onSubmit={formik.handleSubmit}>
      <EducationLevelsList formik={formik} />
      <div className="col-12">
        <br></br>
        <div className="container_btns_step">
            <ButtonPrevStep
                type='button'
                mainPrev={startPrevStep}
            />
            <ButtonNextStep
            type='submit'
            /> 
        </div>
      </div>
    </form>
  )
}

export default StepEducationLevels
