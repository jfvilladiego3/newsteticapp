import React, {useContext} from 'react'

/*Context */
import { EditFormContext } from "../EditFormContext";

/*Components */
import ButtonNextStep from "../../../Components/ButtonNextStep";
import ButtonPrevStep from "../../../Components/ButtonPrevStep";
import StepLanguagesList from './Components/Languajes/StepLanguagesList'

function StepLanguages() {
  const context = useContext(EditFormContext);
  const state = context.state;
  const actions = context.actions;
  const { main } = state;
  const { startNextStep, startPrevStep } = actions
  return (
    <div>
      
      <StepLanguagesList/>
      <div className="col-12">
            <br></br>
            <div className="container_btns_step">
                <ButtonPrevStep
                    type='button'
                    
                    mainPrev={startPrevStep}
                />
                <ButtonNextStep
                type='button'
                main={main}
                nextStep={startNextStep}
                />
            </div>
        </div>
    </div>
  )
}

export default StepLanguages
