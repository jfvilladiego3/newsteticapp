import React, { useState, useContext, useEffect } from "react";
/*Context */
import { EditFormContext } from "../EditFormContext";

/*Boostrap Components */
import { Input } from "reactstrap";
/*Components */
import ButtonNextStep from "../../../Components/ButtonNextStep";
import ButtonPrevStep from "../../../Components/ButtonPrevStep";
import StepFamiliarList from "./Components/Familiars/StepFamiliarList";

function StepFamiliarInformation() {
  const context = useContext(EditFormContext);
  const [haveFamiliars, setHaveFamiliars] = useState();
  const state = context.state;
  const actions = context.actions;
  const { main, user, familiars } = state;
  const {
    startNextStep,
    startPrevStep,
    startDeleteMassieveFamiliar,
    notificationGlobal,
  } = actions;

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    if (familiars.length > 0) {
      setHaveFamiliars(false);
    } else {
      setHaveFamiliars(true);
    }
  }, [familiars]);

  const liveAlone = () => {
    if (!haveFamiliars) {
      if (familiars.length > 0) {
        startNextStep(main + 1);
      } else {
        notificationGlobal("Debe almenos registrar un familiar");
      }
    } else {
      startDeleteMassieveFamiliar(user, familiars, main);
    }
  };

  return (
    <div className="">
      <div className="center-text">
        <h5>
          <strong>Acá debes diligenciar la información de tu núcleo familiar primario</strong>
        </h5>
      </div>

      <StepFamiliarList />
      <div className="col-12">
        <br></br>
        <div className="container_btns_step">
          <ButtonPrevStep type="button" mainPrev={startPrevStep} />
          <ButtonNextStep type="button" liveAlone={liveAlone} />
        </div>
      </div>
    </div>
  );
}

export default StepFamiliarInformation;
