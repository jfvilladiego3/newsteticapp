import React, {useState, useEffect, useReducer, Suspense} from 'react'
/* Context */
import {EditFormContext} from './EditFormContext'
/**Services */
import { 
    getUser, 
    updateUser, 
    getDepartments, 
    getLanguages,
    saveFamiliars, 
    deleteFamiliars,
    saveLanguage,
    deleteLanguage,
    saveEducationLevel,
    deleteEducationLevel
} from "../../Services/EditProfile";
/*Components */
import EditForm from './EditForm'
/*Actions */
import { reducer, initialState } from '../../Flux/Reducers';
import { useActions } from '../../Flux/Store';
import { applyMiddleware } from '../../Flux/Middleware';

/*Components */
import Loanding from '../../Components/Loanding/Loanding'
import NavbarStep from './Components/NavbarStep'

function EditFormWrapper(props) {
    const [ state, dispatch] =  useReducer(reducer, initialState)
    const actions = useActions(state, applyMiddleware(dispatch))

    useEffect(() => {
        const id = props.match.params.id
        actions.startGetDeparment()
        actions.startGetUser(id)
        actions.startGetListLanguages()
        actions.startGetDiseases()
    }, [])

    const { errorUser, loadingUser, loadingDeparments, loadingListLanguages, departmentuser, department } = state
    if(loadingUser || loadingDeparments || loadingListLanguages ) return <Loanding/>
    if(errorUser) return <div> { errorUser } </div> 

    return (
        <Suspense fallback={<Loanding/>}>
            {<EditFormContext.Provider
            value={{
                state,
                actions
            }}
            >
                <NavbarStep/>
                <EditForm/>
            </EditFormContext.Provider>}
        </Suspense>
    )
}

export default EditFormWrapper
