export const NEXT_STEP = "NEXT_STEP";
export const PREV_STEP = "PREV_STEP";
export const UPDATE_USER = "UPDATE_USER";

export const actions = {
    NEXT_STEP,
    PREV_STEP,
    UPDATE_USER
}