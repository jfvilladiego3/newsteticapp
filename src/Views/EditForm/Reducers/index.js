import React, { useReducer } from 'react';

export const NEXT_STEP = "NEXT_STEP";
export const PREV_STEP = "PREV_STEP";
export const UPDATE_USER = "UPDATE_USER";

export const reducer = ( state, action ) => {
    switch (action.type) {
        case NEXT_STEP:
            return{
                main:  action.payload
            }
            break;
        case PREV_STEP:
            return{
                main: action.payload - 1
            }
            break;
        default:
            break;
    }
}