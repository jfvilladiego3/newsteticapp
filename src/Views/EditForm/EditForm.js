import React, {useContext, useEffect } from 'react'

/*Context */
import {EditFormContext} from './EditFormContext'

/*Steps */
import StepAcceptTerms from './Steps/StepAcceptTerms'
import StepPersonalInformation from './Steps/StepPersonalInformation'
import StepFamiliarInformation from './Steps/StepFamiliarInformation'
import StepLanguages from './Steps/StepLanguages'
import StepEducationLevels from './Steps/StepEducationLevels'
import StepHealt from './Steps/StepHealt'
import StepThanks from './Steps/StepThanks'

function EditForm() {
    const context = useContext(EditFormContext)
    const state = context.state
    const { user, main } = state
    return (
        <div className="card-shadow">
            {user && !user.accept_terms == 1 && main == 0 && <StepAcceptTerms/>} 
            {user && user.accept_terms == 1 && main == 0 && <StepPersonalInformation className="fade-in"/>}
            {user && user.accept_terms == 1 && main == 1 && <StepFamiliarInformation className="fade-in" />}
            {user && user.accept_terms == 1 && main == 2 && <StepLanguages className="fade-in" />}
            {user && user.accept_terms == 1 && main == 3 && <StepEducationLevels className="fade-in" />}
            {user && user.accept_terms == 1 && main == 4 && <StepHealt className="fade-in" />}
            {user && user.accept_terms == 1 && main == 5 && <StepThanks className="fade-in" />}
        </div>
    )
}

export default EditForm
