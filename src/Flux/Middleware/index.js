import {
  getUser,
  updateUser,
  getDepartments,
  getLanguages,
  saveFamiliars,
  editFamiliars,
  deleteFamiliars,
  saveLanguage,
  editLanguage,
  deleteLanguage,
  saveEducationLevel,
  deleteEducationLevel,
  getDiseases,
  createUserDiseases,
  sendImageProfile,
} from "../../Services/EditProfile";

import {
  START_GET_USER,
  SUCCESS_GET_USER,
  ERROR_GET_USER,
  START_GET_DEPARMENTS,
  SUCCESS_GET_DEPARMENTS,
  ERROR_GET_DEPARMENTS,
  START_GET_LIST_LANGUAGES,
  SUCCESS_GET_LIST_LANGUAGES,
  ERROR_GET_LIST_LANGUAGES,
  START_UPDATE_USER,
  SUCCESS_UPDATE_USER,
  ERROR_UPDATE_USER,
  START_USER_ACCEPT_TERMS,
  SUCCESS_USER_ACCEPT_TERMS,
  ERROR_USER_ACCEPT_TERMS,
  START_CREATE_FAMILIAR,
  SUCCESS_CREATE_FAMILIAR,
  ERROR_CREATE_FAMILIAR,
  START_EDIT_FAMILIAR,
  SUCCESS_EDIT_FAMILIAR,
  ERROR_EDIT_FAMILIAR,
  START_DELETE_FAMILIAR,
  SUCCESS_DELETE_FAMILIAR,
  ERROR_DELETE_FAMILIAR,
  START_DELETE_MASSIVE_FAMILIAR,
  SUCCESS_DELETE_MASSIVE_FAMILIAR,
  ERROR_DELETE_MASSIVE_FAMILIAR,
  START_CREATE_LANGUAGE,
  SUCCESS_CREATE_LANGUAGE,
  ERROR_CREATE_LANGUAGE,
  START_EDIT_LANGUAGE,
  SUCCESS_EDIT_LANGUAGE,
  ERROR_EDIT_LANGUAGE,
  START_DELETE_LANGUAGE,
  SUCCESS_DELETE_LANGUAGE,
  ERROR_DELETE_LANGUAGE,
  START_GET_EDUCATIONS,
  SUCCESS_GET_EDUCATIONS,
  ERROR_GET_EDUCATIONS,
  START_CREATE_EDUCATION_LEVELS,
  SUCCESS_CREATE_EDUCATION_LEVELS,
  ERROR_CREATE_EDUCATION_LEVELS,
  START_GET_DISEASES,
  SUCCESS_GET_DISEASES,
  ERROR_GET_DISEASES,
  START_CREATE_USER_DISEASE,
  SUCCESS_CREATE_USER_DISEASE,
  ERROR_CREATE_USER_DISEASE,
  START_SAVE_IMAGE_PROFILE,
  SUCCESS_SAVE_IMAGE_PROFILE,
  ERROR_SAVE_IMAGE_PROFILE,
  NOTIFICATION_GLOBAL,
  RESET_STEP,
  NEXT_STEP,
  PREV_STEP,
} from "../../Const/FormEditActions";

export const applyMiddleware = (dispatch) => (action) => {
  /*Users */
  const getUserStart = () => {
    dispatch({ type: START_GET_USER });
  };

  const getUserSuccess = (data) => {
    dispatch({ type: SUCCESS_GET_USER, payload: data });
  };

  const getUserError = (err) => {
    dispatch({ type: ERROR_GET_USER, payload: err });
  };

  /*Deparments */
  const getDeparmentsStart = () => {
    dispatch({ type: START_GET_DEPARMENTS });
  };

  const getDeparmentsSuccess = (data) => {
    dispatch({ type: SUCCESS_GET_DEPARMENTS, payload: data });
  };

  const getDeparmentsError = (err) => {
    dispatch({ type: ERROR_GET_DEPARMENTS, payload: err });
  };

  /*Diseases */
  const getDiseasesStart = () => {
    dispatch({ type: START_GET_DISEASES });
  };

  const getDiseasesSuccess = (data) => {
    dispatch({ type: SUCCESS_GET_DISEASES, payload: data });
  };

  const getDiseasesError = (err) => {
    dispatch({ type: ERROR_GET_DISEASES, payload: err });
  };

  /*Diseases Create*/
  const createDiseasesStart = () => {
    dispatch({ type: START_CREATE_USER_DISEASE });
  };

  const createDiseasesSuccess = (isDisease, diseases) => {
    dispatch({ type: SUCCESS_CREATE_USER_DISEASE, isDisease, diseases });
  };

  const createDiseasesError = (err) => {
    dispatch({ type: ERROR_CREATE_USER_DISEASE, payload: err });
  };

  /* Languajes */
  const getListLanguagesStart = () => {
    dispatch({ type: START_GET_LIST_LANGUAGES });
  };

  const getListLanguagesSuccess = (data) => {
    dispatch({ type: SUCCESS_GET_LIST_LANGUAGES, payload: data });
  };

  const getListLanguagesError = (err) => {
    dispatch({ type: ERROR_GET_LIST_LANGUAGES, payload: err });
  };
  /* UpdateUser */
  const updateUserStart = () => {
    dispatch({ type: START_UPDATE_USER });
  };

  const updateUserSuccess = (data, main) => {
    dispatch({ type: SUCCESS_UPDATE_USER, payload: data, main });
  };

  const updateUserError = (err) => {
    dispatch({ type: ERROR_UPDATE_USER, payload: err });
  };

  /* UserAceptTerms */
  const acceptTermsUserStart = () => {
    dispatch({ type: START_USER_ACCEPT_TERMS });
  };

  const acceptTermsUserSuccess = (data) => {
    dispatch({ type: SUCCESS_USER_ACCEPT_TERMS, payload: data });
  };

  const acceptTermsUserError = (err) => {
    dispatch({ type: ERROR_USER_ACCEPT_TERMS, payload: err });
  };

  /*CreateFamiliar */
  const createFamiliarStart = () => {
    dispatch({ type: START_CREATE_FAMILIAR });
  };

  const createFamiliarSuccess = (data) => {
    dispatch({ type: SUCCESS_CREATE_FAMILIAR, payload: data });
  };

  const createFamiliarError = (err) => {
    dispatch({ type: ERROR_CREATE_FAMILIAR, payload: err });
  };

  /*UpdateFamiliar */
  const updateFamiliarStart = () => {
    dispatch({ type: START_EDIT_FAMILIAR });
  };

  const updateFamiliarSuccess = (data, index) => {
    dispatch({ type: SUCCESS_EDIT_FAMILIAR, payload: data, index });
  };

  const updateFamiliarError = (err) => {
    dispatch({ type: ERROR_EDIT_FAMILIAR, payload: err });
  };

  /*DeleteFamiliar */
  const deleteFamiliarStart = () => {
    dispatch({ type: START_DELETE_FAMILIAR });
  };

  const deleteFamiliarSuccess = (index) => {
    dispatch({ type: SUCCESS_DELETE_FAMILIAR, index });
  };

  const deleteFamiliarError = (err) => {
    dispatch({ type: ERROR_DELETE_FAMILIAR, payload: err });
  };

  /*Delete Massive Familiars */
  const deleteMassiveFamiliarStart = () => {
    dispatch({ type: START_DELETE_MASSIVE_FAMILIAR });
  };

  const deleteMassiveFamiliarSuccess = (main) => {
    dispatch({ type: SUCCESS_DELETE_MASSIVE_FAMILIAR, main });
  };

  const deleteMassiveFamiliarError = (err) => {
    dispatch({ type: ERROR_DELETE_MASSIVE_FAMILIAR, payload: err });
  };

  /*CreateLanguages */
  const createLanguageStart = () => {
    dispatch({ type: START_CREATE_LANGUAGE });
  };

  const createLanguageSuccess = (data) => {
    dispatch({ type: SUCCESS_CREATE_LANGUAGE, payload: data });
  };

  const createLanguageError = (err) => {
    dispatch({ type: ERROR_CREATE_LANGUAGE, payload: err });
  };

  /*EditLanguage */
  const updateLanguageStart = () => {
    dispatch({ type: START_EDIT_LANGUAGE });
  };

  const updateLanguageSuccess = (data, index) => {
    dispatch({ type: SUCCESS_EDIT_LANGUAGE, payload: data, index });
  };

  const updateLanguageError = (err) => {
    dispatch({ type: ERROR_EDIT_LANGUAGE, payload: err });
  };

  /*DeleteLanguage */
  const deleteLanguageStart = () => {
    dispatch({ type: START_DELETE_LANGUAGE });
  };

  const deleteLanguageSuccess = (index) => {
    dispatch({ type: SUCCESS_DELETE_LANGUAGE, index });
  };

  const deleteLanguageError = (err) => {
    dispatch({ type: ERROR_DELETE_LANGUAGE, payload: err });
  };

  /*CreateLanguages */
  const createEducationsLevelsStart = () => {
    dispatch({ type: START_CREATE_EDUCATION_LEVELS });
  };

  const createEducationsLevelsSuccess = (data, user) => {
    dispatch({ type: SUCCESS_CREATE_EDUCATION_LEVELS, payload: data, user });
  };

  const createEducationsLevelsError = (err) => {
    dispatch({ type: ERROR_CREATE_EDUCATION_LEVELS, payload: err });
  };

  /*saveImageProfile */
  const saveImageProfileStart = () => {
    dispatch({ type: START_SAVE_IMAGE_PROFILE });
  };

  const saveImageProfileSuccess = (data) => {
    dispatch({ type: SUCCESS_SAVE_IMAGE_PROFILE, payload: data });
  };

  const saveImageProfileError = (err) => {
    dispatch({ type: ERROR_SAVE_IMAGE_PROFILE, payload: err });
  };

  /*Notification global */
  const notificationGlobal = (message) => {
    dispatch({ type: NOTIFICATION_GLOBAL, payload: message });
  };

  const resetStep = () => {
    dispatch({ type: RESET_STEP });
  };
  /*NextStep */
  const nextStepChange = (data) => {
    dispatch({ type: NEXT_STEP, payload: data });
  };

  /*PrevStep */
  const prevStepChange = () => {
    dispatch({ type: PREV_STEP });
  };

  /*Actions */
  switch (action.type) {
    case START_GET_USER:
      getUserStart();
      return getUser(action.data)
        .then((res) => {
          getUserSuccess(res);
        })
        .catch((err) => {
          getUserError(err);
        });
    case START_GET_DEPARMENTS:
      getDeparmentsStart();
      return getDepartments()
        .then((res) => {
          getDeparmentsSuccess(res);
        })
        .catch((err) => getDeparmentsError(err));
    case START_GET_DISEASES:
      getDiseasesStart();
      return getDiseases()
        .then((res) => {
          getDiseasesSuccess(res);
        })
        .catch((err) => getDiseasesError(err));
    case START_GET_LIST_LANGUAGES:
      getListLanguagesStart();
      return getLanguages()
        .then((res) => {
          getListLanguagesSuccess(res);
        })
        .catch((err) => getListLanguagesError(err));

    case START_UPDATE_USER:
      updateUserStart();
      return updateUser(action.update_user.fk_user_id, action.update_user)
        .then((res) => {
          if (res.status == "ok") {
            updateUserSuccess(action.update_user, action.main);
          } else {
            updateUserError(res.error);
          }
        })
        .catch((err) => {
          console.log(err);
          updateUserError(err);
        });
    case START_USER_ACCEPT_TERMS:
      acceptTermsUserStart();
      return updateUser(action.update_user.fk_user_id, { accept_terms: 1 })
        .then((res) => {
          acceptTermsUserSuccess(action.update_user);
        })
        .catch((err) => acceptTermsUserError(err));
    case START_CREATE_FAMILIAR:
      createFamiliarStart();

      return saveFamiliars(action.familiar)
        .then((res) => {
          createFamiliarSuccess(res.familiars);
        })
        .catch((err) => createFamiliarError(err));
    case START_EDIT_FAMILIAR:
      updateFamiliarStart();

      return editFamiliars(action.id, action.familiar)
        .then((res) => {
          if (res.status == "ok") {
            updateFamiliarSuccess(res.familiar, action.index);
          } else {
            updateFamiliarError(res);
          }
        })
        .catch((err) => updateFamiliarError(err));
    case START_DELETE_FAMILIAR:
      deleteFamiliarStart();
      return deleteFamiliars(action.id)
        .then((res) => {
          deleteFamiliarSuccess(action.index);
        })
        .catch((err) => deleteFamiliarError(err));

    case START_DELETE_MASSIVE_FAMILIAR:
      deleteMassiveFamiliarStart();
      action.familiars.map((familiar) => {
        deleteFamiliars(familiar.id)
          .then((res) => {})
          .catch((err) => console.log(err));
      });
      return deleteMassiveFamiliarSuccess(action.main + 1);

    case START_CREATE_LANGUAGE:
      createLanguageStart();
      return saveLanguage(action.language)
        .then((res) => {
          createLanguageSuccess(res.language);
        })
        .catch((err) => createLanguageError(err));
    case START_EDIT_LANGUAGE:
      updateLanguageStart();
      return editLanguage(action.id, action.language)
        .then((res) => {
          if (res.status == "ok") {
            updateLanguageSuccess(res.language, action.index);
          } else {
            updateLanguageError(res);
          }
        })
        .catch((err) => updateLanguageError(err));
    case START_DELETE_LANGUAGE:
      deleteLanguageStart();
      return deleteLanguage(action.id)
        .then((res) => {
          deleteLanguageSuccess(action.index);
        })
        .catch((err) => deleteLanguageError(err));

    case START_CREATE_EDUCATION_LEVELS:
      createEducationsLevelsStart();
      return saveEducationLevel({
        fk_user_id: action.fk_user_id,
        levels: action.educations,
      })
        .then((res) => {
          updateUser(action.fk_user_id, action.update_user)
            .then((res) => {
              if (res.status == "ok") {
                // updateUserSuccess(action.update_user, action.main)
              } else {
                // updateUserError(res.error)
              }
            })
            .catch((err) => {
              console.log(err);
            });
          createEducationsLevelsSuccess(action.educations, action.update_user);
        })
        .catch((err) => createEducationsLevelsError(err));

    case START_CREATE_USER_DISEASE:
      createDiseasesStart();
      return createUserDiseases(
        action.fk_user_id,
        { is_disease: action.isDisease },
        action.main,
        action.diseases
      )
        .then((res) => {
          if (res.status == "ok") {
            createDiseasesSuccess(action.isDisease, action.diseases);
          } else {
            createDiseasesError(res);
          }
        })
        .catch((err) => createEducationsLevelsError(err));

    case START_SAVE_IMAGE_PROFILE:
      saveImageProfileStart();
      return sendImageProfile(action.fk_user_id, action.file)
        .then((res) => {
          console.log(res.secure_url);
          if (res.secure_url) {
            const image_profile = res.secure_url;
            updateUser(action.fk_user_id, { image_profile }, 1)
              .then((res) => {
                if (res.status == "ok") {
                  saveImageProfileSuccess(image_profile);
                } else {
                  saveImageProfileError(res);
                }
              })
              .catch((err) => {
                console.log(err);
                saveImageProfileError(err);
              });
          }
        })
        .catch((err) => saveImageProfileError(err));

    case NOTIFICATION_GLOBAL:
      return notificationGlobal(action.message);
    case RESET_STEP:
      return resetStep();
    case NEXT_STEP:
      return nextStepChange(action.data);
    case PREV_STEP:
      return prevStepChange();
    default:
      dispatch(action);
  }
};
