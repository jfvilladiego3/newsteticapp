import {
  START_GET_USER,
  START_GET_DEPARMENTS,
  START_GET_DISEASES,
  START_GET_LIST_LANGUAGES,
  START_UPDATE_USER,
  START_USER_ACCEPT_TERMS,
  START_CREATE_FAMILIAR,
  START_EDIT_FAMILIAR,
  START_DELETE_FAMILIAR,
  START_CREATE_USER_DISEASE,
  NEXT_STEP,
  PREV_STEP,
  START_CREATE_LANGUAGE,
  START_EDIT_LANGUAGE,
  START_DELETE_LANGUAGE,
  START_DELETE_MASSIVE_FAMILIAR,
  START_CREATE_EDUCATION_LEVELS,
  START_SAVE_IMAGE_PROFILE,
  NOTIFICATION_GLOBAL,
  RESET_STEP,
} from '../../Const/FormEditActions'

export const useActions = (state, dispatch) => ({ 
  startGetUser: data => { 
    dispatch({ type: START_GET_USER, data }) 
  },
  startGetDeparment: data => { 
    dispatch({ type: START_GET_DEPARMENTS }) 
  },
  startGetDiseases: data => { 
    dispatch({ type: START_GET_DISEASES }) 
  },
  startGetListLanguages: data => { 
    dispatch({ type: START_GET_LIST_LANGUAGES }) 
  },
  startUpdateUser: (data, main) => { 
    dispatch({ type: START_UPDATE_USER, update_user: data, main }) 
  },
  startUserAcceptTerms: data => { 
    dispatch({ type: START_USER_ACCEPT_TERMS, update_user: data }) 
  },
  startCreateFamiliar: data => { 
    dispatch({ type: START_CREATE_FAMILIAR, familiar: data }) 
  },
  startEditFamiliar: (id, familiar, index) => { 
    dispatch({ type: START_EDIT_FAMILIAR, id, familiar, index }) 
  },
  startDeleteFamiliar: (id, index) => { 
    dispatch({ type: START_DELETE_FAMILIAR,  id, index }) 
  },
  startDeleteMassieveFamiliar: (user, familiars, main) => { 
    dispatch({ type: START_DELETE_MASSIVE_FAMILIAR,  user, familiars, main }) 
  },
  startCreateLanguage: language => { 
    dispatch({ type: START_CREATE_LANGUAGE, language }) 
  },
  startEditLanguage: (id, language, index) => { 
    dispatch({ type: START_EDIT_LANGUAGE, language, id, index }) 
  },
  startDeleteLanguage: (id, index) => { 
    dispatch({ type: START_DELETE_LANGUAGE,  id, index }) 
  },
  startCreateEducationLevels: (fk_user_id , educations, update_user) => {
    dispatch({type: START_CREATE_EDUCATION_LEVELS, educations, fk_user_id, update_user })
  },
  startCreateDiseases: (fk_user_id , isDisease, diseases, main) => {
    dispatch({type: START_CREATE_USER_DISEASE, fk_user_id, isDisease, diseases, main })
  },
  startSaveImage: (fk_user_id , file) => {
    dispatch({type: START_SAVE_IMAGE_PROFILE, fk_user_id, file })
  },
  notificationGlobal: (message) => { 
    dispatch({ type: NOTIFICATION_GLOBAL, message}) 
  },
  startResetStep: () => { 
    dispatch({ type: RESET_STEP }) 
  },
  startNextStep: data => { 
    dispatch({ type: NEXT_STEP, data: data }) 
  },
  startPrevStep: () => { 
    dispatch({ type: PREV_STEP}) 
  },
})