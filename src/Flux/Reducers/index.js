import { store } from 'react-notifications-component';
import { notificatinoSuccess, notificatinoDanger } from '../../Const/NotificationConfig'
import {
  START_GET_USER,
  SUCCESS_GET_USER,
  ERROR_GET_USER,

  START_GET_DEPARMENTS,
  SUCCESS_GET_DEPARMENTS,
  ERROR_GET_DEPARMENTS,

  START_GET_LIST_LANGUAGES,
  SUCCESS_GET_LIST_LANGUAGES,
  ERROR_GET_LIST_LANGUAGES,

  START_UPDATE_USER,
  SUCCESS_UPDATE_USER,
  ERROR_UPDATE_USER,

  START_USER_ACCEPT_TERMS,
  SUCCESS_USER_ACCEPT_TERMS,
  ERROR_USER_ACCEPT_TERMS,

  START_CREATE_FAMILIAR,
  SUCCESS_CREATE_FAMILIAR,
  ERROR_CREATE_FAMILIAR,

  START_EDIT_FAMILIAR,
  SUCCESS_EDIT_FAMILIAR,
  ERROR_EDIT_FAMILIAR,

  START_CREATE_LANGUAGE,
  SUCCESS_CREATE_LANGUAGE,
  ERROR_CREATE_LANGUAGE,

  START_EDIT_LANGUAGE,
  SUCCESS_EDIT_LANGUAGE,
  ERROR_EDIT_LANGUAGE,

  START_DELETE_LANGUAGE,
  SUCCESS_DELETE_LANGUAGE,
  ERROR_DELETE_LANGUAGE,

  START_DELETE_MASSIVE_FAMILIAR,
  SUCCESS_DELETE_MASSIVE_FAMILIAR,
  ERROR_DELETE_MASSIVE_FAMILIAR,

  START_CREATE_EDUCATION_LEVELS,
  SUCCESS_CREATE_EDUCATION_LEVELS,
  ERROR_CREATE_EDUCATION_LEVELS,

  START_CREATE_USER_DISEASE,
  SUCCESS_CREATE_USER_DISEASE,
  ERROR_CREATE_USER_DISEASE,

  START_SAVE_IMAGE_PROFILE,
  SUCCESS_SAVE_IMAGE_PROFILE,
  ERROR_SAVE_IMAGE_PROFILE,

  NOTIFICATION_GLOBAL,
  RESET_STEP,
  NEXT_STEP,
  PREV_STEP,
  START_DELETE_FAMILIAR,
  SUCCESS_DELETE_FAMILIAR,
  ERROR_DELETE_FAMILIAR,
  START_GET_DISEASES,
  ERROR_GET_DISEASES,
  SUCCESS_GET_DISEASES
  } from '../../Const/FormEditActions'

const initialState = { 
  user: {},
  userWP: {},
  loadingUser: false,
  errorUser: null,
  main: 0,

  familiars: [],
  loadingFamiliars: false,
  errorFamiliar: null,

  userDiseases: [],

  languages: [],
  loadingLanguages: false,
  errorLanguages: null,

  educations: [],
  loandingEducations: false,
  errorEducations: null,

  listLanguages: [],
  loandingListLanguages: false,
  errorListLanguages: null,

  departments: [],
  loandingDepartments: false,
  errorDeparments: null,

  diseases: [],
  loandingDiseases: false,
  errorDiseases: null,

  createDiseases: [],
  loandingCreateDiseases: false,
  errorCreateDiseases: null,

  loandingUpdateUser: false,
  errorUpdateUser: null,

  loandingCreateFamiliar: false,
  errorCreateFamiliar: null,

  loandingEditFamiliar: false,
  errorEditFamiliar: null,

  loandingCreateLanguage: false,
  errorCreateLanguage:  null,

  loandingEditLanguage: false,
  errorEditLanguage: null,

  loandingDeleteLanguage: false,
  errorDeleteLanguage:  null,

  loandingDeleteFamiliar: false,
  errorDeleteFamiliar: null,

  loandingCreateEducationLevels: false,
  errorCreateEducationLevels:  null,

  loandingDeleteMassiveFamiliar: false,
  errorDeleteMassiveFamiliar: null,

  loandingSaveImage: false,
  errorSaveImage: null,

}

const reducer = (state = initialState, action) => { 
 switch (action.type) { 
  /*User */
  case START_GET_USER: 
    return { ...state, loadingUser: true} 
  case SUCCESS_GET_USER:
    let listDepartments = null
    if(state.departments && state.departments.length !== 0 && action.payload.user?.residence_department !== null){
      listDepartments = state.departments
      .filter(department => {
        return department.departamento.toUpperCase() == action.payload.user.residence_department.toUpperCase();
      })
      
    }

    if(listDepartments && listDepartments.length === 0)
      action.payload.user.residence_department = ''

      
    return { ...state, 
      loadingUser: false, 
      user: action.payload.user, 
      familiars: action.payload.ext_family_members ,
      languages: action.payload.ext_languages,
      userWP: action.payload.user_wp,
      educations: action.payload.educations_leves,
      userDiseases: action.payload.user_diseases
    } 
  case ERROR_GET_USER: 
    return { ...state, loadingUser: false, errorUser: action.payload } 
  /*Deparment */  
  case START_GET_DEPARMENTS: 
    return { ...state, loadingDepartments: true} 
  case SUCCESS_GET_DEPARMENTS:
      return { ...state, loadingDepartments: false, departments: action.payload } 
  case ERROR_GET_DEPARMENTS: 
    return { ...state, loadingDepartments: false, errorDepartments: action.payload } 
  /*Deparment */  
  case START_GET_DISEASES: 
    return { ...state, loandingDiseases: true} 
  case SUCCESS_GET_DISEASES:
      return { ...state, loandingDiseases: false, diseases: action.payload } 
  case ERROR_GET_DISEASES: 
    return { ...state, loandingDiseases: false, errorDiseases: action.payload } 

  /*Diseases */  
  case START_GET_LIST_LANGUAGES: 
    return { ...state, loandingListLanguages: true} 
  case SUCCESS_GET_LIST_LANGUAGES:
      return { ...state, loadingListLanguages: false, listLanguages: action.payload } 
  case ERROR_GET_LIST_LANGUAGES: 
    return { ...state, loadingListLanguages: false, errorListLanguages: action.payload } 
  
  /*UpdateUsers */
  case START_UPDATE_USER: 
    return { ...state, loandingUpdateUser: true} 
  case SUCCESS_UPDATE_USER:
      notificatinoSuccess.title = 'Datos Personales'
      notificatinoSuccess.message = 'Datos actualizados de manera exitosa'
      store.addNotification(notificatinoSuccess);
      let phone = action.payload?.phone
      
      if(!phone.length <= 10 && !phone.length >= 1){
        phone = ''
      }

      return { ...state, 
        loandingUpdateUser: false, 
        main: action.main,
        userWP: {
          ...state.userWP,  
          user_email:action.payload?.email 
        },
        user: {
        ...state.user,
        gender: action.payload?.gender,
        id_card_rh: action.payload?.id_card_rh,
        birth_date: action.payload?.birth_date,
        civil_status: action.payload?.civil_status,
        type_employee: action.payload.type_employee,
        id_card_expedition_place: action.payload?.id_card_expedition_place,
        child_number: action.payload?.child_number != null ? action.payload.child_number : "",
        have_sons: action.payload?.child_number > 0 ? 1 : 0,
        address: action.payload?.address,
        department: action.payload?.department,
        city: action.payload?.city,
        neighborhood: action.payload?.neighborhood,
        home_type: action.payload?.home_type,
        military_card: action.payload?.military_card,
        military_card_class: action.payload?.military_card_class,
        military_card_district: action.payload?.military_card_district,
        phone: action.payload?.phone != null ? action.payload.phone : "",
        local_phone: action.payload?.local_phone != null ? action.payload.local_phone : "",
        private_transport: action.payload?.private_transport != null ? action.payload.private_transport : "",
        smoker: action.payload?.smoker != null ? action.payload.smoker : "",
        alcoholic: action.payload?.alcoholic != null ? action.payload.alcoholic : ""
      }}
  case ERROR_UPDATE_USER: 
    notificatinoDanger.title = 'Información Personal'
    notificatinoDanger.message = 'Los datos no han podido ser almacenados intente de nuevo'
    store.addNotification(notificatinoDanger);
    return { ...state, loandingUpdateUser: false, errorUpdateUser: action.payload } 
  
  /*CreateFamiliar */
  case START_CREATE_FAMILIAR: 
    return { ...state, loandingCreateFamiliar: true} 
  case SUCCESS_CREATE_FAMILIAR:
      notificatinoSuccess.title = 'Información familiar'
      notificatinoSuccess.message = 'Datos actualizados de manera exitosa'
      store.addNotification(notificatinoSuccess);
      return { ...state, loandingCreateFamiliar: false, familiars: [...state.familiars, action.payload] } 
  case ERROR_CREATE_FAMILIAR: 
    notificatinoDanger.title = 'Información familiar'
    notificatinoDanger.message = 'Los datos no han podido ser almacenados intente de nuevo'
    store.addNotification(notificatinoDanger);
    return { ...state, loandingCreateFamiliar: false, errorCreateFamiliar: action.payload } 
  
  /*EditFamiliar */
  case START_EDIT_FAMILIAR: 
    return { ...state, loandingEditFamiliar: true, errorEditFamiliar: null} 
  case SUCCESS_EDIT_FAMILIAR:
      notificatinoSuccess.title = 'Información familiar'
      notificatinoSuccess.message = 'Datos actualizados de manera exitosa'
      store.addNotification(notificatinoSuccess);
      let newFamiliars = state.familiars
      newFamiliars[action.index] = action.payload
      return { ...state, loandingEditFamiliar: false, familiars: newFamiliars } 
  case ERROR_EDIT_FAMILIAR: 
      notificatinoDanger.title = 'Información Familiar'
      notificatinoDanger.message = 'Los datos no se han podido actualizar intente de nuevo'
      store.addNotification(notificatinoDanger);
    return { ...state, loandingEditFamiliar: false, errorEditFamiliar: action.payload } 
  
  /*DeleteFamiliar */
  case START_DELETE_FAMILIAR: 
    return { ...state, loandingDeleteFamiliar: true} 
  case SUCCESS_DELETE_FAMILIAR:
    notificatinoSuccess.title = 'Información familiar'
    notificatinoSuccess.message = 'Datos eliminados de manera exitosa'
    store.addNotification(notificatinoSuccess);
    let deleteFamiliars = state.familiars
    deleteFamiliars.splice(action.index, 1)
    return { ...state, loandingDeleteFamiliar: false, familiars: deleteFamiliars } 
  case ERROR_DELETE_FAMILIAR: 
    return { ...state, loandingDeleteFamiliar: false, errorDeleteFamiliar: action.payload } 

  case START_DELETE_MASSIVE_FAMILIAR:
    return {...state, loandingDeleteMassiveFamiliar: true, errorDeleteMassiveFamiliar: null  }
  case SUCCESS_DELETE_MASSIVE_FAMILIAR:
    notificatinoSuccess.title = 'Información familiar'
    notificatinoSuccess.message = 'Datos actualizados de manera exitosa'
    store.addNotification(notificatinoSuccess);
    return {...state,  loandingDeleteMassiveFamiliar: false, familiars: [], main: action.main } 
  case ERROR_DELETE_MASSIVE_FAMILIAR:
    notificatinoDanger.title = 'Información familiar'
    notificatinoDanger.message = 'Los datos no han podido ser almacenados intente de nuevo'
    store.addNotification(notificatinoDanger);
    return {...state,  loandingDeleteMassiveFamiliar: true, errorDeleteMassiveFamiliar: action.payload }
  /*CreateLanguage */
  case START_CREATE_LANGUAGE: 
    return { ...state, loandingCreateLanguage: true} 
  case SUCCESS_CREATE_LANGUAGE:
    notificatinoSuccess.title = 'Idiomas'
    notificatinoSuccess.message = 'Datos actualizados de manera exitosa'
    store.addNotification(notificatinoSuccess);
    return { ...state, loandingCreateLanguage: false, languages: [...state.languages, action.payload] } 
  case ERROR_CREATE_LANGUAGE: 
    notificatinoDanger.title = 'Idiomas'
    notificatinoDanger.message = 'Los datos no han podido ser almacenados intente de nuevo'
    store.addNotification(notificatinoDanger);
    return { ...state, loandingCreateLanguage: false, errorCreateLanguage: action.payload } 

  /*EditLanguage */
  case START_EDIT_LANGUAGE: 
    return { ...state, loandingEditLanguage: true, errorEditLanguage: null} 
  case SUCCESS_EDIT_LANGUAGE:

      notificatinoSuccess.title = 'Idiomas'
      notificatinoSuccess.message = 'Datos actualizados de manera exitosa'
      store.addNotification(notificatinoSuccess);
      let newLanguage = state.languages
      newLanguage[action.index] = action.payload
      return { ...state, loandingEditLanguage: false, languages: newLanguage } 
  case ERROR_EDIT_LANGUAGE: 
      notificatinoDanger.title = 'Idiomas'
      notificatinoDanger.message = 'Los datos no se han podido actualizar intente de nuevo'
      store.addNotification(notificatinoDanger);
    return { ...state, loandingEditLanguage: false, errorEditLanguage: action.payload } 

  /*DeleteFamiliar */
  case START_DELETE_LANGUAGE: 
    return { ...state, loandingDeleteLanguage: true} 
  case SUCCESS_DELETE_LANGUAGE:
    notificatinoSuccess.title = 'Idiomas'
    notificatinoSuccess.message = 'Datos eliminados de manera exitosa'
    store.addNotification(notificatinoSuccess);
    let deleteLanguage = state.languages
    deleteLanguage.splice(action.index, 1)
    return { ...state, loandingDeleteLanguage: false, languages: deleteLanguage } 
  case ERROR_DELETE_LANGUAGE: 
    return { ...state, loandingDeleteLanguage: false, errorDeletelanguage: action.payload } 

  /*Create EducationLevels */
  case START_CREATE_EDUCATION_LEVELS:
    return {...state, loandingCreateEducationLevels: true  }
  case SUCCESS_CREATE_EDUCATION_LEVELS:
    notificatinoSuccess.title = 'Formación'
    notificatinoSuccess.message = 'Datos actualizados de manera exitosa'
    store.addNotification(notificatinoSuccess);
    return {
      ...state, 
      loandingCreateEducationLevels: false, 
      educations: action.payload, 
      user: {
        ...state.user,
        currently_grade:  action.user.currently_grade ,
        currently_institution:   action.user.currently_institution ,
        currently_taken:   action.user.currently_taken 
      },
      main: state.main + 1  }
  case ERROR_CREATE_EDUCATION_LEVELS:
    return {...state, loandingCreateEducationLevels: false, errorCreateEducationLevels: action.payload}
    
  /*Create Diseases */
  case START_CREATE_USER_DISEASE:
    return {...state, loandingCreateDiseases: true  }
  case SUCCESS_CREATE_USER_DISEASE:
    notificatinoSuccess.title = 'Estado de salud'
    notificatinoSuccess.message = 'Datos actualizados de manera exitosa'
    store.addNotification(notificatinoSuccess);
    return {
      ...state, 
      loandingCreateDiseases: false, 
      userDiseases: action.diseases, 
      user: {
        ...state.user,
        is_disease: action.isDisease == 1 ? 1 : 0
      },
      main: state.main + 1  
    }
  case ERROR_CREATE_USER_DISEASE:
    return {...state, loandingCreateDiseases: false, errorCreateDiseases: action.payload}

  /*Create Diseases */
  case START_SAVE_IMAGE_PROFILE:
    return {...state, loandingSaveImage: true  }
  case SUCCESS_SAVE_IMAGE_PROFILE:
    notificatinoSuccess.title = 'Imagen perfil'
    notificatinoSuccess.message = 'Foto almacenada de manera exitosa'
    store.addNotification(notificatinoSuccess);
    return {...state, loandingSaveImage: false, user: {
      ...state.user,
      image_profile: action.payload
    },  }
  case ERROR_SAVE_IMAGE_PROFILE:
    notificatinoDanger.title = 'Imagen perfil'
    notificatinoDanger.message = 'La foto no pudo ser almacenada intente de nuevo'
    store.addNotification(notificatinoDanger);
    return {...state, loandingSaveImage: false, errorSaveImage: action.payload}

  /*UpdateUserAccepTerms */
  case START_USER_ACCEPT_TERMS: 
    return { ...state, loandingUpdateUser: true} 
  case SUCCESS_USER_ACCEPT_TERMS:
      return { ...state, loandingUpdateUser: false, user: {
        ...state.user,
        accept_terms: 1,
      }} 
  case ERROR_USER_ACCEPT_TERMS: 
    return { ...state, loandingUpdateUser: false, errorUpdateUser: action.payload } 
  /*Step */
  case NOTIFICATION_GLOBAL:
    notificatinoDanger.title = ''
    notificatinoDanger.message = action.payload
    store.addNotification(notificatinoDanger);
    return {...state}
  case RESET_STEP:
    return {...state, main: 0}
  case NEXT_STEP:
    return{...state, main:  action.payload}
  case PREV_STEP:
      return{...state, main: state.main - 1}
  default:
    return{...state}
}} 

export { initialState, reducer }
