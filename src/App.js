import React, {Fragment, useState} from 'react';
import { BrowserRouter as Router, HashRouter } from 'react-router-dom'
import './App.css';
/*Routes */
import Routes from './Routes'
import ReactNotifications from 'react-notifications-component';
import ModalContainer from './Components/Modals/ModalContainer';

function App() {

  const [show, setShow] = useState(false)

  return (
    <div className="">
      <HashRouter>
        <Fragment>
          <ReactNotifications />
          <ModalContainer 
          show={show}
          setShow={setShow}
          >
            hola mundo
          </ModalContainer>
          <Routes/>
        </Fragment>
      </HashRouter>
    </div>
  );
}

export default App;
