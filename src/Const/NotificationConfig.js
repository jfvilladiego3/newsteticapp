export const notificatinoSuccess = {
  title: '',
  message: '',
  type: 'success',                         // 'default', 'success', 'info', 'warning'
  container: 'top-right',                // where to position the notifications
  animationIn: ["animated", "bounceIn"],     // animate.css classes that's applied
  animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
  dismiss: {
    duration: 3000 
  }
}

export const notificatinoDanger = {
  title: '',
  message: '',
  type: 'danger',                         // 'default', 'success', 'info', 'warning'
  container: 'top-right',                // where to position the notifications
  animationIn: ["animated", "bounceIn"],     // animate.css classes that's applied
  animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
  dismiss: {
    duration: 5000 
  }
}
