import React, { useState, useEffect } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

// import { jsx, css, Global, ClassNames } from "@emotion/core";
import styled from 'styled-components'

const ModalContainerStyled = styled.div`
  .w3-modal{
    display: block;
  }
  .w3-modal-content{
    border-radius: 5px;
  }
  .modal-container{
    display: grid;
    gap: 1em;
    grid-template-columns: 1fr;
    .header{
      display: flex;
      justify-content: space-between;
      border-bottom: 1px solid #d2d7da;
      .icon-close{
        font-size: 20px;
        cursor: pointer;
      }
    }
    .footer{
      border-top: 1px solid #d2d7da;
      padding-top: 10px;
      text-align: right;
    }
  }
`;

function ModalContainerDelete({removeItem, identification, index, toggle, show, title, message, setShow}) {
  return (
    <ModalContainerStyled>
      <div className="w3-container">
        {show && <div id="myModal" className="w3-modal">
          <div className="w3-modal-content">
            <div className="w3-container modal-container">
              <div className="header">
                <div>
                  {title}
                </div>
                <span
                  onClick={() => setShow(false)}
                  className="icon-close"
                >
                  &times;
                </span>
              </div>

              <div className="content" >
                {message}
              </div>

              <div className="footer" >
                <Button color="success" onClick={ () => {setShow(false); removeItem(identification, index)} } >Eliminar</Button>{' '}
                <Button color="warning" onClick={ () => setShow(false)}>Cancelar</Button>
              </div>
            </div>
          </div>
        </div>}
      </div>
    </ModalContainerStyled>
  );
}

export default ModalContainerDelete;
