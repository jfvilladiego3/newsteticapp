import React, {useState} from 'react'


import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
function ModalDelete({removeItem, identification, index, toggle, isOpen, title, message}) {
  
  
  return (
    <div>
      <Modal isOpen={isOpen} toggle={toggle} >
        <ModalHeader toggle={toggle}> {title} </ModalHeader>
        <ModalBody>
          {message}
        </ModalBody>
        <ModalFooter>
          <Button color="success" onClick={ () => {toggle(); removeItem(identification, index)} } >Eliminar</Button>{' '}
          <Button color="warning" onClick={toggle}>Cancelar</Button>
        </ModalFooter>
      </Modal>
    </div>
  )
}

export default ModalDelete
