import React, { useState, useEffect } from "react";
import { Overlay } from "react-portal-overlay";
// import { jsx, css, Global, ClassNames } from "@emotion/core";
import styled from 'styled-components'

const ModalContainerStyled = styled.div`
  .w3-modal{
    display: block;
  }
  .w3-modal-content{
    border-radius: 5px;
  }
  .modal-container{
    display: grid;
    gap: 1em;
    grid-template-columns: 1fr;
    .header{
      display: flex;
      justify-content: space-between;
      border-bottom: 1px solid #d2d7da;
      .icon-close{
        font-size: 20px;
        cursor: pointer;
      }
    }
    .footer{
      border-top: 1px solid #d2d7da;
      padding-top: 10px;
      text-align: right;
    }
  }
`;

function ModalContainer({show, setShow, children, title, onClickClose}) {

  const actionOnCloseModal = () => {
    if (onClickClose !== undefined && onClickClose !== undefined) {
      onClickClose();
    }

    setShow(false);
  }

  return (
    <ModalContainerStyled>
      <div className="w3-container">
        {show && <div id="myModal" className="w3-modal">
          <div className="w3-modal-content">
            <div className="w3-container modal-container">
              <div className="header">
                <div>
                  {title}
                </div>
                <span
                  onClick={() => actionOnCloseModal()}
                  className="icon-close"
                >
                  &times;
                </span>
              </div>

              <div className="content" >
                {children}
              </div>

              <div className="footer" >
                <button 
                type='button' 
                className="btn btn-warning" 
                onClick={() => actionOnCloseModal()}
                >
                  Cerrar
                </button>
              </div>
            </div>
          </div>
        </div>}
      </div>
    </ModalContainerStyled>
  );
}

export default ModalContainer;
