import React, {useState} from 'react'
import IosArrowForward from 'react-ionicons/lib/IosArrowForward'
function ButtonNextStep(props) {
  const { type, nextStep, main, liveAlone } = props
  const [hover, setHover] = useState(false)

  const defaultFuncion = () => {
    return true
  }

    return (
        <button 
        type={type ? type: 'button'}
        className="btn btn-success btn-step"
        onMouseEnter={() => setHover(true)} 
        onMouseLeave={() =>setHover(false)}
        onClick={() => {
          nextStep ?  nextStep(main+1): defaultFuncion()
          liveAlone ? liveAlone() : defaultFuncion()
        }}
        >
           {/* Continuar<IosArrowForward fontSize="30px" color={hover ? 'white':"#28a745"}  className='btn-step__next' /> */}
          Continuar<IosArrowForward fontSize="30px" color={'white'}  className='btn-step__next' />
        </button>
    )
}

export default ButtonNextStep
