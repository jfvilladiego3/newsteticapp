import React from 'react'
import IosArrowBack from 'react-ionicons/lib/IosArrowBack'

function ButtonPrevStep(props) {
    const { type, mainPrev } = props
    const defaultFuncion = () => {
        return true
    }
    return (
        <button 
        onClick={() => {mainPrev ?  mainPrev() : defaultFuncion()}}
        type={type ? type: 'button'}
        className="btn btn-light">
           <IosArrowBack fontSize="30px" color="black"  className='btn-step__next' /> Atras
        </button>
    )
}

export default ButtonPrevStep
