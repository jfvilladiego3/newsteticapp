import React, {useState, useEffect} from 'react'
import styled from "styled-components";
import Loanding from "../Loanding/Loanding";


const ImageProfileStyled = styled.div`
  text-align: center;
  cursor: pointer;
  img {
    /* border-radius: 150px;
    border: solid 3px #39A4B4;
    width: 150px;
    padding: 20px; */
    width: 150px;

  }
`;
//foldervcb

function ImageProfile({id, saveFile, error, defaultImage, loanding}) {
  const imgProfile = 'https://res.cloudinary.com/da1ciyo26/image/upload/v1594090567/qj7vzfmcwr333vacm6ik.png'
  const [openCrop, setOpenCrop] = useState(false)
  const [fileImg, setFileImg] = useState(imgProfile)
    const inputRef = React.createRef();
    const onFileChange = (event) =>{
        if(event.target.files && event.target.files[0]){
            //Image preview
            saveFile(id,event.target.files[0])
            const reader = new FileReader();
            reader.onload = e => {
                setFileImg(reader.result)
            }
            reader.readAsDataURL(event.target.files[0])
          }
    } 
    const _handleClick = (e) =>{
        inputRef.current.click();
    } 

    useEffect(() => {
      if(!error && defaultImage){
        setFileImg(defaultImage)
      }else{
        setFileImg(imgProfile)
      }
    }, [])

  return (
    <ImageProfileStyled>
      {loanding && 'Cargando...'  }
      {!loanding &&  <div>
      
      <img onClick={(e) => _handleClick(e)} src={fileImg} alt="avatar"/>
      <input type="file"
            accept="image/*"
            ref={inputRef}
            onChange={(e) => onFileChange(e)}
            className="d-none"
            />
      </div>
      }
    </ImageProfileStyled>
    
  )
}

export default ImageProfile
