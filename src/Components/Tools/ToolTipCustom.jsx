import React, { useState } from 'react';
import { Tooltip } from 'reactstrap';

function ToolTipCustom({identification, message, placement}) {
  const [tooltipOpen, setTooltipOpen] = useState(false);

  const toggle = () => setTooltipOpen(!tooltipOpen);
  return (
    <div>
      <Tooltip 
      placement={placement ? placement : "right"} 
      isOpen={tooltipOpen} 
      target={identification} 
      toggle={toggle}>
        {message}
      </Tooltip>
    </div>
  )
}

export default ToolTipCustom
