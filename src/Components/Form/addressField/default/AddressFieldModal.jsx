import React from 'react'

import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';


function AddressFieldModal({isOpen, toggle, title,  onClickSuccess, children, size}) {
  return (
    <Modal isOpen={isOpen} toggle={toggle} size={size}>
        <ModalHeader toggle={toggle}> {title} </ModalHeader>
        <ModalBody className="modal_address_field" >
          {children}
        </ModalBody>
        <ModalFooter>
          <Button color="warning" onClick={toggle}>Cancelar</Button>
        </ModalFooter>
      </Modal>
  )
}

export default AddressFieldModal
