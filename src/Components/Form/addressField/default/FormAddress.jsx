import React, {useState, useEffect} from "react";

import ModalContainer from "../../../Modals/ModalContainer";

/** Form storage */
import {
  TypeRoutes,
  TypeLetter,
  TypeQuadrant,
} from "../../../../Views/EditForm/FormStorage";

/** Utils */
import { toInputLimitCharactersNumbers, toInputUppercaseWhitNumbersAndSpace } from "../../../../Utils/inputUppercase";

/** Hooks */
import { useFormAddress } from "./useFormAddress";

/** Components */
import ErrorField from "../../ErrorField";

function FormAddress({ showModal, setShowModal, toggle, setAddress, name }) {
  const { formik, concatAddress } = useFormAddress(
    setAddress,
    setShowModal,
    name
  );

  const { values, errors, touched, handleChange, handleBlur } = formik;

  const onClickClose = () => {
    formik.resetForm();
  }

  return (
    <ModalContainer
      show={showModal}
      setShow={setShowModal}
      title={"Dirección:"}
      onClickClose={onClickClose}
    >
      <form className="form-address" onSubmit={formik.handleSubmit}>
        <div className="form-address__input-container">
          <div>
            <label htmlFor="">Tipo Vía (*)</label>
            <select
              name="typeRoute"
              value={values.direction}
              onChange={handleChange}
              onBlur={handleBlur}
              className="form-control"
            >
              {TypeRoutes.map((route) => (
                <option key={route.value} value={route.value}>
                  {route.name}
                </option>
              ))}
            </select>
            <ErrorField
              touched={touched.typeRoute}
              message={errors.typeRoute}
            />
          </div>

          <div>
            <label htmlFor="">Número (*)</label>
            <input
              type="text"
              className="form-control"
              name="numberRoute"
              onChange={handleChange}
              onBlur={handleBlur}
              onInput={toInputLimitCharactersNumbers}
              value={values.numberRoute}
            />
            <ErrorField
              touched={touched.numberRoute}
              message={errors.numberRoute}
            />
          </div>

          <div>
            <label htmlFor="">Letra</label>
            <select
              name="letterRoute"
              value={values.direction}
              onChange={handleChange}
              onBlur={handleBlur}
              className="form-control"
            >
              {TypeLetter.map((letter, index) => (
                <option key={index} value={letter.value}>
                  {letter.name}
                </option>
              ))}
            </select>
            <ErrorField
              touched={touched.letterRoute}
              message={errors.letterRoute}
            />
          </div>

          <div>
            <label htmlFor="">Cuadrante</label>
            <select
              name="quadrantA"
              value={values.direction}
              onChange={handleChange}
              onBlur={handleBlur}
              className="form-control"
            >
              {TypeQuadrant.map((quadrant, index) => (
                <option key={index} value={quadrant.value}>
                  {quadrant.name}
                </option>
              ))}
            </select>
            <ErrorField
              touched={touched.quadrantA}
              message={errors.quadrantA}
            />
          </div>

          <div>
            <label htmlFor="">Número (*)</label>
            <input
              type="text"
              className="form-control"
              name="numberQuadrantA"
              onChange={handleChange}
              onBlur={handleBlur}
              onInput={toInputLimitCharactersNumbers}
              value={values.numberQuadrantA}
            />
            <ErrorField
              touched={touched.numberQuadrantA}
              message={errors.numberQuadrantA}
            />
          </div>

          <div>
            <label htmlFor="">Letra</label>
            <select
              name="letterQuadrantA"
              value={values.direction}
              onChange={handleChange}
              onBlur={handleBlur}
              className="form-control"
            >
              {TypeLetter.map((letter, index) => (
                <option key={index} value={letter.value}>
                  {letter.name}
                </option>
              ))}
            </select>
            <ErrorField
              touched={touched.letterQuadrantA}
              message={errors.letterQuadrantA}
            />
          </div>

          <div>
            <label htmlFor="">Cuadrante</label>
            <select
              name="quadrantB"
              value={values.direction}
              onChange={handleChange}
              onBlur={handleBlur}
              className="form-control"
            >
              {TypeQuadrant.map((quadrant, index) => (
                <option key={quadrant.value ? quadrant.value : index} value={quadrant.value}>{quadrant.name}</option>
              ))}
            </select>
            <ErrorField
              touched={touched.quadrantB}
              message={errors.quadrantB}
            />
          </div>

          <div>
            <label htmlFor="">Número (*)</label>
            <input
              type="text"
              className="form-control"
              name="numberQuadrantB"
              onChange={handleChange}
              onBlur={handleBlur}
              onInput={toInputLimitCharactersNumbers}
              value={values.numberQuadrantB}
            />
            <ErrorField
              touched={touched.numberQuadrantB}
              message={errors.numberQuadrantB}
            />
          </div>
        </div>

        <br />

        <div>
          <label htmlFor="">
            Información adicional. Ejemplo:(Apartamento, Edificio, Finca)
          </label>
          <input
            type="text"
            className="form-control"
            name="description"
            onChange={handleChange}
            onBlur={handleBlur}
            onInput={toInputUppercaseWhitNumbersAndSpace}
            value={values.description}
          />
          <ErrorField
            touched={touched.description}
            message={errors.description}
          />
        </div>

        <br />

        <div className="preview-address alert alert-secondary" role="alert">
          {concatAddress(values)}
        </div>

        <div className="accept-btn">
          <button type="submit" className="btn btn-success">
            Aceptar
          </button>
        </div>
      </form>
    </ModalContainer>
  );
}

export default FormAddress;
