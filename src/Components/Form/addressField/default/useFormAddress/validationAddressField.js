import * as Yup from "yup";

const ValidationSchema = Yup.object().shape({
  typeRoute: Yup.string()
    .required("Este campo es requerido"),
  numberRoute: Yup.string()
    .required("Este campo es requerido"),
  numberQuadrantA: Yup.string()
    .required("Este campo es requerido"),
  numberQuadrantB: Yup.string()
    .required("Este campo es requerido")
});

export default ValidationSchema;

