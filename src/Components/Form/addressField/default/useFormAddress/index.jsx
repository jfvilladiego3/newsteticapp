import {useEffect, useState} from 'react'
/* Formik */
import { useFormik } from "formik";
import ValidationSchema from "./validationAddressField";

export const  useFormAddress = (setAddress, setShowModal, name) => {
  const initialValues = {
    typeRoute: '',
    numberRoute: '',
    letterRoute:'',
    quadrantA: '',
    numberQuadrantA: '',
    letterQuadrantA: '',
    quadrantB: '',
    numberQuadrantB: '',
    description: ''
  }

  const formik = useFormik({
    validationSchema: ValidationSchema,
    initialValues,
    onSubmit: (values) => {
      const newDirection = concatAddress(values)

      setAddress(name, newDirection);
      setShowModal(false);
    },
  });

  const concatAddress = (values) => {
    return Object.values(values).join(' ');
  }

  return {
    formik,
    concatAddress
  }
}
