import React, { useState, Fragment } from "react";
import styled from "styled-components";

import FormAddress from './default/FormAddress'

const AddressFieldStyled = styled.div``;

const AddressField = (props) => {
  const {
    name,
    value,
    onChange,
    onBlur,
    setFieldValue,
  } = props
  
  const [showModal, setShowModal] = useState(false);
  const toggle = () => setShowModal(!showModal);

  return (
    <Fragment  >
      <div onClick={() => setShowModal(!showModal)}>
        <label htmlFor="">
        Dirección (*)
        Haz clic <strong  className="action-text">aquí</strong> para actualizar
        </label>
        <input 
        type="text" 
        className="form-control" 
        name={name}
        onChange={onChange}
        onBlur={onBlur}
        value={value}
        disabled />
      </div>
      <FormAddress  
      name={name}
      setAddress={setFieldValue}
      showModal={showModal}
      toggle={toggle}
      setShowModal={setShowModal}
      /> 
    </Fragment>
  );
};

export default AddressField;
