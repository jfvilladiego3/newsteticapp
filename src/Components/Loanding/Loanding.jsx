import React from 'react'
import LoandingGif from '../../images/Loanding.gif'

function Loanding() {
  return (
    <div className="loanding" >
      <div className="loanding__container" >
        {/* <h1>Loanding...</h1> */}
        <img src={LoandingGif} alt="loanding" />
      </div>
    </div>
  )
}

export default Loanding
