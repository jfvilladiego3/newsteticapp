import React, {useContext} from 'react'

import {EditFormContext} from '../EditFormContext'
import MdPerson from 'react-ionicons/lib/MdPerson'
import MdPeople from 'react-ionicons/lib/MdPeople'
import IosListBox from 'react-ionicons/lib/IosListBox'
import IosChatbubbles from 'react-ionicons/lib/IosChatbubbles'
import MdMedical from 'react-ionicons/lib/MdMedical'

import ImagePerfil from '../../../Components/ImageProfile/ImageProfile'


function NavbarStep() {
  const context = useContext(EditFormContext)
  const state = context.state
  const actions = context.actions
  const { main, user, loandingSaveImage, errorSaveImage} = state
  
  return (
    <div className="navbar-step" >
      <ImagePerfil
      defaultImage={user.image_profile}
      error={errorSaveImage}
      id={user.fk_user_id}
      saveFile={actions.startSaveImage}
      loanding={loandingSaveImage}
      />
        <div className="navbar-step__title" >
          <h5>
          <strong>
            {user.complete_name}
          </strong>
          </h5>
        </div>
      <div className="navbar-step__items ">
        <div className={"item "+
          (main == 0 ? 'item-active success' : '')
        }>
          
          <MdPerson
          fontSize="45px" color={main == 0 ? 'white': '#39A4B4'}   
          className='circle-icon' />
          <h6 className="title">Información personal</h6>
        </div>
        <div className= {"item "+
          (main == 1 ? 'item-active' : '')
        }>
          <MdPeople
          fontSize="45px" color={main == 1 ? 'white': '#39A4B4'}   
          className='circle-icon' />
          <h6 className="title">Información familiar</h6>
        </div>
        <div className={"item "+
          (main == 2 ? 'item-active' : '')
        }>
          <IosChatbubbles
          fontSize="45px" color={main == 2 ? 'white': '#39A4B4'}   
          className='circle-icon' />
          <h6 className="title">Idiomas</h6>
        </div>
        <div className={"item "+
          (main == 3 ? 'item-active' : '')
        }>
          <IosListBox
          fontSize="45px" color={main == 3 ? 'white': '#39A4B4'}   
          className='circle-icon' />
          <h6 className="title">Formación</h6>
        </div>
        <div className={"item "+
          (main == 4 ? 'item-active' : '')
        }>
          <MdMedical
          fontSize="45px" color={main == 4 ? 'white': '#39A4B4'}   
          className='circle-icon' />
          <h6 className="title">Estado de salud</h6>
        </div>
      </div>
        
    </div>
  )
}

export default NavbarStep
