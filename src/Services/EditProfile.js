 import axios from 'axios';

const URI = document.getElementById('getUrl').value
const API = URI+'user-profile/'
const APIBASE = URI

/*GetUser */
export const getUser = (id) => {
  return  axios.get(`${API}getuser?id=${id}`)
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}

/*Edit User */
export const updateUser = (id, data, main) => {
  return  axios.post(`${API}edit-profile?id=${id}`,{data:data, main: main})
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}

/*deparments */
export const getDepartments=  () => {
  return  axios.get(`${API}data-colombia`)
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}

/*ListLanguage */
export const getLanguages=  () => {
  return  axios.get(`${API}data-languajes`)
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}

/* Familiars */
export const saveFamiliars = (data) => {
  return  axios.post(`${API}save-familiar`,{data:data})
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}

export const editFamiliars = (id,data) => {
  return  axios.post(`${API}edit-familiar?id=${id}`,{data:data})
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}

export const deleteFamiliars = (id) => {
  return  axios.post(`${API}delete-familiar?id=${id}`)
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}

/*Languages */
export const saveLanguage = (data) => {
  return  axios.post(`${API}save-language`,{data:data})
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}

export const editLanguage = (id,data) => {
  return  axios.post(`${API}edit-language?id=${id}`,{data:data})
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}


export const deleteLanguage = (id) => {
  return  axios.post(`${API}delete-language?id=${id}`)
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}

/*Education leves */
export const saveEducationLevel = (data) => {
  return  axios.post(`${API}save-education-level`,{data:data})
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}

export const deleteEducationLevel = (id) => {
  return  axios.post(`${API}delete-education-level?id=${id}`)
          .then(response=>{
              return response.data
          })
          .catch(error => console.log(error))
}

/** Get Diseases */
export const getDiseases = () => {
  return  axios.get(`${APIBASE}diseases`)
  
          .then(response=>{
              return response.data[0]
          })
          .catch(error => console.log(error))
}

/*Create User Desiraes */
export const createUserDiseases = (id, data, main, diseases) => {
  return  axios.post(`${API}edit-profile?id=${id}`,{data, main, diseases})
          .then(response=>{
            return response.data
          })
          .catch(error => console.log(error))
}

/*Send File */
export const sendImageProfile = (id, file) => {
  let formData = new FormData();
  formData.append("file", file);
  formData.append("upload_preset", 'foldervcb');
  const headers = {'Content-Type': 'multipart/form-data'}
  return  axios.post(`https://api.cloudinary.com/v1_1/da1ciyo26/upload`, formData, headers)
          .then(response=>{
            return response.data
          })
          .catch(error => console.log(error))
}
/*Save FIle */
// export const sendImageProfile = (id, file) => {
//   let formData = new FormData();
//   formData.append("file", file);
//   const headers = {'Content-Type': 'multipart/form-data'}
//   return  axios.post(`${API}edit-profile/image?id=${id}`, file, headers)
//           .then(response=>{
//             return response.data
//           })
//           .catch(error => console.log(error))
// }
