export const toInputUppercase = e => {
  const regex =  /[^A-Za-zñÑ ]/;
  e.target.value = ("" + e.target.value).toUpperCase();
  if( regex.test(e.target.value.slice(-1) !== " " && e.target.value  )  ){
    const str = e.target.value.substring(0, e.target.value.length - 1);
    e.target.value = str
  }
};

export const toInputUppercaseWhitNumbersAndSpace = e => {
  const regex =  /[^A-Za-z0-9ñÑ ]/;
  e.target.value = ("" + e.target.value).toUpperCase();
  if( regex.test(e.target.value.slice(-1) !== " " && e.target.value  )  ){
    const str = e.target.value.substring(0, e.target.value.length - 1);
    e.target.value = str
  }
};

export const toInputUppercaseWhitNumbers = e => {
  const regex =  /[^A-Za-z0-9ñÑ]/;
  e.target.value = ("" + e.target.value).toUpperCase();
  if( regex.test(e.target.value.slice(-1) !== " " && e.target.value  )  ){
    const str = e.target.value.substring(0, e.target.value.length - 1);
    e.target.value = str
  }
};

export const toInputOnlyNumbers = e => {
  const regex =  /[^0-9]/;
  e.target.value = ("" + e.target.value).toUpperCase();
  if( regex.test(e.target.value.slice(-1) !== " " && e.target.value  )  ){
    const str = e.target.value.substring(0, e.target.value.length - 1);
    e.target.value = str
  }
};

export const toInputLimitCharactersNumbers = e => {
  const regex =  /[^0-9]/;
  e.target.value = ("" + e.target.value).toUpperCase();
  if(  e.target.value.length > 10 || regex.test(e.target.value.slice(-1) !== " " && e.target.value  ) ){
    const str = e.target.value.substring(0, e.target.value.length - 1);
    e.target.value = str
  }
};

export const toInputLimitCharactersPhone = e => {
  const regex =  /[^0-9]/;
  e.target.value = ("" + e.target.value).toUpperCase();
  if(  e.target.value.length > 20 || regex.test(e.target.value.slice(-1) !== " " && e.target.value  ) ){
    const str = e.target.value.substring(0, e.target.value.length - 1);
    e.target.value = str
  }
};

export const toInputLimitCharactersLocalPhone = e => {
  const regex =  /[^0-9]/;
  e.target.value = ("" + e.target.value).toUpperCase();
  if(  e.target.value.length > 10 || regex.test(e.target.value.slice(-1) !== " " && e.target.value  ) ){
    const str = e.target.value.substring(0, e.target.value.length - 1);
    e.target.value = str
  }
};

export const toInputLimitTwoCharactersNumbers = e => {
  const regex =  /[^0-9]/;
  e.target.value = ("" + e.target.value).toUpperCase();
  if(  e.target.value.length > 2 || regex.test(e.target.value.slice(-1) !== " " && e.target.value  ) ){
    const str = e.target.value.substring(0, e.target.value.length - 1);
    e.target.value = str
  }
};
