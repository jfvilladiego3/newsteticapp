import React,{useEffect} from 'react'
import {Route, Switch, HashRouter, useHistory } from 'react-router-dom';
import EditFormWrapper from '../Views/EditForm/EditFormWrapper'

function Routes() {
  let history = useHistory();

  useEffect(() => {
    const id = document.getElementById('getUserId').value
    if(id !== '' && id !== null && id !== undefined ){
      history.push("/"+id);
    }
  }, [])

  return (
    <Switch>
      <Route exact path="/:id" component={EditFormWrapper} />
    </Switch>
  )
}

export default Routes
