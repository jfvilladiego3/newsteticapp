# Data update user form NewStetic

With this modules the users can update data,
https://newstetic.vcb.com.co/perfiledit/#/[id]

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn dist`

Builds the app for production to the `dist` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Deployment

first compile project with 

### `yarn dist`

this must be crate a dist folder, 
now for ssh you have to pass all files of folder dist to cms in next ubication

### `newstetic/cms/wp-content/themes/aardvark/js/form-edit-profile`

you must make sure that file exist, if not exist you can copy and paste next code
### `newstetic/cms/wp-content/themes/aardvark-child/page-perfiledit.php`

and have this code
```php
<?php get_header(); ?>
<?php esc_url( admin_url( 'admin-post.php' )); ?>

<?php 
    if(is_user_logged_in()){
        $cu = wp_get_current_user();
    }else{
        wp_redirect( home_url());
    }
?>
<style>
.container_iframe{
    margin-left: 1%;
}
#root{
    margin-bottom: 200px;
}

</style>

<link rel="apple-touch-icon" href="<?php echo esc_url( get_template_directory_uri().'/js/form-edit-profile/logo192.png')?>" />
<link rel="manifest" href="<?php echo esc_url( get_template_directory_uri().'/js/form-edit-profile/manifest.json')?>" />

<input type="hidden" id="getUrl" value="<?php echo home_url().'/newstetic_api/web/index.php/api/'?>">
<input type="hidden" id="getUserId" value="<?php echo $cu->ID ?>">

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


<noscript>You need to enable JavaScript to run this app.</noscript>
<div id="root"></div>

<script src="<?php echo esc_url( get_template_directory_uri().'/js/form-edit-profile/bundle.js')?>" ></script>

<?php get_footer(); ?>

```
